@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Requisição</h3>
                    <h3 class="p-2">
                        Processo: {{$requisicao->COD_PROCESSO_F}} - Etapa: {{nomeEtapa($requisicao->COD_ETAPA_F)}} - Status: {{nomeStatus($requisicao->IDE_FINALIZADO)}}
                    </h3>
                </div>

                <div class="card-body">
                    <h6 class="mb-3">
                        <a href="{{route('admin.home')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </h6>
                    
                    <div class="form-row">

                        <div class="form-group col-md-5">
                            <label>Empresa</label>
                            <input type="text" class="form-control" value="{{ $requisicao->EMPRESA }}" readonly>
                        </div>
                        <div class="form-group col-md-5">
                            <label>Associado</label>
                            <input type="text" class="form-control" value="{{ $requisicao->NOME }}" readonly>
                        </div>
                        <div class="form-group col-md-2">
                            <label>CPF</label>
                            <input type="text" class="form-control" value="{{ $requisicao->CPF }}" readonly>
                        </div>
                        
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-8">
                            <label>Email</label>
                            <input type="text" class="form-control" value="{{ $requisicao->EMAIL }}" readonly>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Celular</label>
                            <input type="text" class="form-control" value="{{ $requisicao->CELULAR }}" readonly>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Telefone</label>
                            <input type="text" class="form-control" value="{{ $requisicao->TELEFONE }}" readonly>
                        </div>
                        
                    </div>

                    <div class="form-row">
                        @for ($i = 1; $i <= 3; $i++)
                            @if (isset($empresa->{"personalizado".$i}) && $empresa->{"personalizado".$i})
                        <div class="form-group col-md-3">
                            <label>{{ ucwords(str_replace('_', ' ', $empresa->{"personalizado".$i})) }}</label>
                            <input type="text" class="form-control" value="{{ $requisicao->{"PERSONALIZADO".$i} }}" readonly>
                        </div>
                            @endif
                        @endfor
                        
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-3">
                            <label>SKU</label>
                            <input type="text" class="form-control" value="{{ $requisicao->SKU }}" readonly>
                        </div>
                        <div class="form-group col-md-9">
                            <label>SKU</label>
                            <input type="text" class="form-control" value="{{ $requisicao->NOME_PRODUTO }}" readonly>
                        </div>

                    </div>
                    
                    <div class="form-row">
                        
                        <div class="form-group col-md-3">
                            <label>Ação requisição</label>
                            <input type="text" class="form-control" value="{{ $requisicao->STATUS }}" readonly>
                        </div>

                        <div class="form-group col-md-3">
                            <label>Ticket</label>
                            <input type="text" class="form-control" value="{{ $requisicao->TICKET }}" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Url Agendamento</label>
                            <input type="text" class="form-control" value="{{ $requisicao->URL_AGENDAMENTO }}" readonly>
                        </div>
                    
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="textareaObservacap">Observação solicitante</label>
                            <textarea name="observacao" class="form-control" rows="4" readonly>{{ $requisicao->OBSERVACAO }}</textarea>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="textareaObservacap">Observação analista</label>
                            @if ($requisicao->COD_ETAPA_F == 5)
                            <form action="" method="POST">
                                
                                @csrf
                                <input type="hidden" name="codProcesso[0]" value="{{$requisicao->COD_PROCESSO_F}}">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <textarea name="observacao" class="form-control" rows="4" cols="80" placeholder="Observação">{{ old('observacao') }}</textarea>
                                    </div>
                                </div>

                                <div class="d-flex flex-wrap bd-highlight mb-3">
                                    <div class="bd-highlight">
                                        
                                    </div>
                                    <div class="bd-highlight">
                                        <button type="submit" class="btn btn-success mb-1" data-toggle="modal" data-target="#modalConfirm" data-acao="APROVAR" data-url="{{ route('admin.requisicao.acao', 'Aprovar') }}">
                                            Aprovar
                                        </a>
                                    </div>
                                    <div class="ml-2 bd-highlight">
                                        <button type="submit" class="btn btn-danger mb-1" data-toggle="modal" data-target="#modalConfirm" data-acao="REJEITAR" data-url="{{ route('admin.requisicao.acao', 'Rejeitar') }}">
                                            Rejeitar
                                        </a>
                                    </div>
                                </div>

                            </form>
                            @else
                            <textarea name="observacao" class="form-control" rows="4" readonly>{{ $requisicao->OBSERVACAO_ANALISE }}</textarea>
                            @endif
                        </div>
                    </div>
                    
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@if ($requisicao->COD_ETAPA_F == 5)
    @push('scripts-footer')
    <script type="text/javascript">
        $(document).ready(function($){
            $("form").submit(function() {
                $("#modalConfirm").modal("hide");
                $('#modalLoader').modal('show');

                var btn = $(this).find("button[type=submit]:focus" );

                $('form').attr('action', btn.data("url"));
            });
        });
    </script>
    @endpush
@endif