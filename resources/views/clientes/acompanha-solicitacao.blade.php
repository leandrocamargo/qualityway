@extends('layouts.cliente')

@section('content')
<div class="form-detail h-100">
    <h2>Preencha o formulário para acompanhar sua solicitação</h2>
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <form action="{{route('cliente.requisicao.acompanha.do', $empresa->nome_url)}}" method="POST">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="inputCpf" class="required">CPF</label>
                <input type="text" name="cpf" class="form-control" value="{{ old('cpf') }}" data-mask="000.000.000-00" data-mask-reverse="true">
            </div>
            <div class="form-group col-md-3">
                <label for="inputCelular" class="required">Celular</label>
                <input type="text" name="celular" class="form-control" value="{{ old('celular') }}" data-mask="(00) 00000-0000">
            </div>

            <div class="form-group col-md-6">
                <label for="inputEmail" class="required">Email</label>
                <input type="text" name="email" class="form-control" value="{{ old('email')}}">
            </div>
        </div>
        
        <button type="submit" class="btn btn-success mt-5">Verificar</button>
        <a href="{{route('cliente.home', $empresa->nome_url)}}" class="btn btn-secondary mt-5">Voltar</a>
    </form>
</div>    
@endsection