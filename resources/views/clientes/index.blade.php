@extends('layouts.cliente')

@section('content')

<form class="form-detail h-100" action="{{(isset($doPreAprovado) ? route('cliente.requisicao.do', [$empresa->nome_url, $produto->sku]) : route('cliente.requisicao.verify', $empresa->nome_url) )}}" method="post">
    @if (isset($doPreAprovado))
    <input type="hidden" name="produto_id" value="{{$produto->id}}">
    <input type="hidden" name="source" value="1">
    @endif
    <h3 class="mb-3">
        Formulário de requisição
        
        <a href="{{route("cliente.requisicao.acompanha", $empresa->nome_url)}}" class="btn btn-success float-right">Acompanhe sua solicitação</a>
    </h2>


    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    
    @csrf
    <div class="form-row">
        <div class="form-group col-md-7">
            <label for="inputNome" class="required">Nome</label>
            <input type="text" name="nome" class="form-control" value="{{ old('nome')}}">
        </div>
        <div class="form-group col-md-5">
            <label for="inputCpf" class="required">CPF</label>
            <input type="text" name="cpf" class="form-control" value="{{ old('cpf') }}" data-mask="000.000.000-00" data-mask-reverse="true">
        </div>
    </div>
    
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="inputEmail" class="required">Email</label>
            <input type="text" name="email" class="form-control" value="{{ old('email')}}">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-5">
            <label for="inputCelular" class="required">Celular</label>
            <input type="text" name="celular" class="form-control" value="{{ old('celular') }}" data-mask="(00) 00000-0000">
        </div>

        
        <div class="form-group col-md-5">
            <label for="inputTelefone">Telefone</label>
            <input type="text" name="telefone" class="form-control" value="{{ old('telefone') }}" data-mask="(00) 0000-0000">
        </div>

    </div>
    
    <div class="form-row">
        @for ($i = 1; $i <= 3; $i++)
            @if ($empresa->{"personalizado".$i})
        <div class="form-group col-md-4">
            <label for="inputPersonalizado{{$i}}" class="{{( ($empresa->{"valida_personalizado".$i} || $empresa->{"obriga_personalizado".$i}) ? 'required' : '')}}">{{ ucwords(str_replace('_', ' ', $empresa->{"personalizado".$i})) }}</label>
            <input type="text" name="personalizado{{$i}}" class="form-control" value="{{ (isset($preAprovado)) ? $preAprovado->{"personalizado".$i} : old( "personalizado".$i ) }}">
        </div>
            
            @endif
        @endfor 
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="textareaObservacao">Observação</label>
            <textarea name="observacao" class="form-control" rows="4" cols="80">{{ old('observacao') }}</textarea>
        </div>
    </div>

    <button type="submit" class="btn btn-primary mt-3">Prosseguir</button>
</form>
@endsection

@push('scripts-footer')
<script>
$(document).ready(function($) {    
    $('form').on('submit', function(e){
        event.preventDefault();
        $('#modalLoader').modal('show');
        $(this).unbind('submit').submit();
    });
});
</script> 
@endpush