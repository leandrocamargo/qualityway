@extends('layouts.cliente')

@section('content')
<div class="form-detail">
    <a href="{{url()->previous()}}" class="btn btn-secondary mb-4">
        Voltar
    </a>

    @if (isset($status))
    <h2>{{$status}}</h2>

    {!! $texto !!}
    
    <a href="{{route("cliente.requisicao.acompanha", $empresa->nome_url)}}" class="btn btn-success mt-5">Acompanhe sua solicitação</a>
    @else
    <div class="table-responsive" style="height: 70vh;">
        <table class="table table-sm table-hover table-bordered table-striped text-center">
            <thead>
            <tr>
                <th class="align-middle">Solicitação</th>
                <th class="align-middle">Data</th>
                <th class="align-middle">Status</th>
                <th class="align-middle">Produto</th>
                <th class="align-middle">Observação</th>
                <th class="align-middle">Ticket</th>
                <th class="align-middle">Url Agendamento/Desconto</th>
            </tr>
            </thead>
            <tbody>  
                @forelse ($requisicoes as $requisicao) 
                <tr>
                    <th scope="row" class="align-middle">{{$requisicao->COD_PROCESSO_F}}</th>
                    <td class="align-middle">{{ \Carbon\Carbon::parse($requisicao->DAT_DATA)->format("d/m/y G:i") }}</td>
                    <td class="align-middle">
                        @if ($requisicao->IDE_FINALIZADO == "C")
                            <span class="text-danger">Cancelado</span>
                        @elseif ($requisicao->COD_ETAPA_F == 1)
                            <span class="text-success">Pré-aprovado</span>
                        @elseif ($requisicao->COD_ETAPA_F == 3)
                            <span class="text-success">Aprovado</span>
                        @elseif ($requisicao->COD_ETAPA_F == 4)
                            <span class="text-danger">Rejeitado</span>
                        @else
                            <span class="text-primary">Em análise</span>
                        @endif
                    </td>
                    <td class="align-middle">{{$requisicao->produto->produto}}</td>
                    <td class="align-middle">{{$requisicao->OBSERVACAO_ANALISE}}</td>
                    <td class="align-middle">{{$requisicao->TICKET}}</td>
                    <td class="align-middle">
                        @if (Str::of($requisicao->URL_AGENDAMENTO)->trim() != "")
                        <a href="{{$requisicao->URL_AGENDAMENTO}}" class="btn btn-link">Link {{($requisicao->TICKET ? 'agendamento' : 'desconto')}}</a>
                        @endif
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="7">Não localizamos nenhuma requisição com os dados fornecidos!</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    @endif

    
</div>    
@endsection