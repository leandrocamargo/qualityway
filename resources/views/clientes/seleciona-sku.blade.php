@extends('layouts.cliente')

@section('content')
<form class="form-detail h-100" action="{{route('cliente.requisicao.do', $empresa->nome_url)}}" method="post">
    <input type="hidden" name="psBio" value="{{$request->psBio}}">
    <input type="hidden" name="cpf" value="{{ $request->cpf }}">
    <input type="hidden" name="nome" value="{{ $request->nome }}">
    <input type="hidden" name="email" value="{{ $request->email }}">
    <input type="hidden" name="celular" value="{{ $request->celular }}">
    <input type="hidden" name="telefone" value="{{ $request->telefone }}">
    <input type="hidden" name="observacao" value="{{ $request->observacao }}">
    <input type="hidden" name="atendimento" value="{{ $request->atendimento }}">
    @csrf
    @if (isset($request->personalizado1))
    <input type="hidden" name="personalizado1" value="{{ $request->personalizado1 }}">
    @endif
    @if (isset($request->personalizado2))
    <input type="hidden" name="personalizado2" value="{{ $request->personalizado2 }}">
    @endif
    @if (isset($request->personalizado3))
    <input type="hidden" name="personalizado3" value="{{ $request->personalizado3 }}">
    @endif
    
    <h2>Escolha o produto que deseja</h2>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="inputSKU">Produto</label>
            <select class="form-control" name="produto_id">
                <option value>Selecione uma opção</option>
                @foreach ($produtos as $produto)
                <option value="{{$produto->id}}" data-sku="{{ $produto->sku }}">{{ $produto->produto }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <button type="submit" class="btn btn-primary mt-3">Efetuar requisição</button>
</form>
@endsection

@push('scripts-footer')
    <script>
        $(document).ready(function($) { 
            $('form').on('submit', function(e){
                $('form').attr('action', $('form').attr('action') + "/" + $('form option:selected').data('sku'));
            });
        });
    </script>
@endpush