@extends('layouts.cliente')

@section('content')
<div class="form-detail h-100">
    @if ($ticket["ticket"])
    <h3>O numero do seu ticket é:</h3>
    <h2><b>{{$ticket["ticket"]}}</b></h2>

    <h5>Você será redirecionado para a página de agendamento da Valid em 10 segundos.</h5>
    <h5>Ou se prefirir acesse o link abaixo para acessar imediatamente.</h5>
    @else
    <h3>Temos um cupom de desconto.</h3>

    <h5>Você será redirecionado para a página de deconto da Valid em 10 segundos.</h5>
    <h5>Ou se prefirir acesse o link abaixo para acessar imediatamente.</h5>
    @endif

    <a href="{{$ticket["url"]}}" class="btn btn-success mt-5">Link {{($ticket['ticket'] ? 'agendamento' : 'desconto')}}</a>

</div>    
@endsection

@push('scripts-footer')
    <script>
        $(document).ready(function($) { 
            var delay = 10000; 
            var url = '{{$ticket["url"]}}'
            setTimeout(function(){ window.location = url; }, delay);
        });
    </script>
@endpush