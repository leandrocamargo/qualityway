@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <h2 class="mb-4">Requisições</h2>


                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <div class="d-flex flex-wrap justify-content-between bd-highlight mb-3">
                        <div class="p-2 bd-highlight">
                            <a href="{{route("admin.produtos.index")}}" class="btn btn-primary">Produtos</a>
                        </div>
                        <div class="p-2 bd-highlight">
                            <a href="{{route("admin.tickets.index")}}" class="btn btn-primary">Tickets</a>        
                        </div>
                        <div class="p-2 bd-highlight">
                            <a href="{{route("admin.empresas.index")}}" class="btn btn-primary">Empresas</a>
                        </div>
                        <div class="p-2 bd-highlight">
                            <a href="{{route("admin.pre-aprovados.index")}}" class="btn btn-primary">Pré-aprovados</a>
                        </div>
                    </div>

                    <hr class="mt-4">

                    <div class="d-flex flex-wrap bd-highlight mb-3">
                        <div class="p-2 bd-highlight">
                            <a href="" class="btn btn-success btn-sm mb-1" data-toggle="modal" data-target="#modalConfirm" data-acao="APROVAR" data-url="{{ route('admin.requisicao.acao', 'Aprovar') }}">
                                Aprovar
                            </a>
                        </div>
                        <div class="p-2 bd-highlight">
                            <a href="" class="btn btn-danger btn-sm mb-1" data-toggle="modal" data-target="#modalConfirm" data-acao="REJEITAR" data-url="{{ route('admin.requisicao.acao', 'Rejeitar') }}">
                                Rejeitar
                            </a>
                        </div>
                    </div>

                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">Filtro</h5>
                            <form id="formFiltro" class="form">
                                <div class="d-flex flex-wrap justify-content-center bd-highlight mb-3">

                                    <div class="col-md-2">
                                        <select class="form-control" name="filtrarPor">
                                            <option value>Selecione uma opção</option>
                                            <option value='COD_PROCESSO_F' {{($filtro["filtrarPor"] == "COD_PROCESSO_F") ? "selected" : "" }}>Cód. Processo</option>
                                            <option value='DAT_DATA' {{($filtro["filtrarPor"] == "DAT_DATA") ? "selected" : "" }}>Data</option>
                                            <option value='EMPRESA' {{($filtro["filtrarPor"] == "EMPRESA") ? "selected" : "" }}>Empresa</option>
                                            <option value='NOME' {{($filtro["filtrarPor"] == "NOME") ? "selected" : "" }}>Associado</option>
                                            <option value='STATUS' {{($filtro["filtrarPor"] == "STATUS") ? "selected" : "" }}>Ação</option>
                                            <option value='SKU' {{($filtro["filtrarPor"] == "SKU") ? "selected" : "" }}>Sku</option>
                                            <option value='NOME_PRODUTO' {{($filtro["filtrarPor"] == "NOME_PRODUTO") ? "selected" : "" }}>Produto</option>
                                            <option value='IDE_ETAPA_FINAL' {{($filtro["filtrarPor"] == "IDE_ETAPA_FINAL") ? "selected" : "" }}>Status</option>
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <select class="form-control" name="operadorFiltro">
                                            <option value='=' {{($filtro["operadorFiltro"] == "=") ? "selected" : "" }}>É igual a...</option>
                                            <option value='!=' {{($filtro["operadorFiltro"] == "!=") ? "selected" : "" }}>É diferente de...</option>
                                            <option value='0' {{($filtro["operadorFiltro"] == "0") ? "selected" : "" }}>Começa com...</option>
                                            <option value='1' {{($filtro["operadorFiltro"] == "1") ? "selected" : "" }}>Termina com...</option>
                                            <option value='2' {{($filtro["operadorFiltro"] == "2") ? "selected" : "" }}>Contém...</option>
                                            <option value='3' {{($filtro["operadorFiltro"] == "3") ? "selected" : "" }}>Não contém...</option>
                                        </select>
                                    </div>

                                    <div class="col-md-7">
                                        <input type="text" name="valorFiltro" class="form-control" value="{{ $filtro["valorFiltro"] }}">
                                    </div>

                                    <div class="col-md-1">
                                        <button type="submit" class="btn btn-dark">Filtrar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <form id="table" action="" method="POST">
                        @csrf
                        <input type="hidden" name="observacao">
                            <table class="table table-borderless table-striped table-hover table-sm" data-sorting="true">
                                <thead>
                                    <tr>
                                        <th>@sortablelink('COD_PROCESSO_F', '#')</th>
                                        <th>&nbsp<input class="form-check-input" type="checkbox" id="selectAll"></th>
                                        <th>Data</th>
                                        <th>@sortablelink('EMPRESA', 'Empresa')</th>
                                        <th>@sortablelink('NOME', 'Associado')</th>
                                        <th>@sortablelink('SKU', 'Sku')</th>
                                        <th>@sortablelink('NOME_PRODUTO', 'Produto')</th>
                                        <th>@sortablelink('TICKET', 'Ticket')</th>
                                        <th>@sortablelink('STATUS', 'Status')</th>
                                        <th>Atualização</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($requisicoes as $requisicao)
                                    <tr style="{{ ($requisicao->IDE_FINALIZADO == "C" ? 'background-color: #ffd9d7 !important;' : '') }}" data-link="{{(isset($requisicao->COD_PROCESSO_F) ? route('admin.requisicao.show', $requisicao->COD_PROCESSO_F) : '' )}}">
                                        <th scope="row">{{$requisicao->COD_PROCESSO_F}}</th>
                                        <td>
                                            @if ( $requisicao->COD_ETAPA_F == 5 )
                                            &nbsp<input name="codProcesso[]" class="form-check-input" type="checkbox" value="{{$requisicao->COD_PROCESSO_F}}">
                                            @endif
                                        </td>
                                        <td>{{ \Carbon\Carbon::parse($requisicao->DAT_DATA)->format("d/m/y G:i") }}</td>
                                        <td>{{ Str::limit($requisicao->EMPRESA, 10) }}</td>
                                        <td>{{$requisicao->NOME}}</td>
                                        <td>{{$requisicao->SKU}}</td>
                                        <td>{{ $requisicao->NOME_PRODUTO }}</td>
                                        <td>{{$requisicao->TICKET}}</td>
                                        <td>{{nomeEtapa($requisicao->COD_ETAPA_F)}}</td>
                                        <td>{{ \Carbon\Carbon::parse($requisicao->DAT_GRAVACAO)->format("d/m/y G:i") }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </form>
                    </div>
                    
                    {{ $requisicoes->onEachSide(0)->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts-footer')
    @component('components.table-click')  
    @endcomponent
    <script type="text/javascript">
        $(document).ready(function($){
            $('#selectAll').click(function(e){    
                var table= $(e.target).closest('table');
                $('td input:checkbox',table).prop('checked',this.checked);
            });
        });
    </script>
@endpush

@component('components.modal')
    
@endcomponent

@component('components.preloader')
            
@endcomponent