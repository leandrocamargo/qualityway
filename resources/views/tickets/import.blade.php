@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2 class="mb-4">Importar tickets</h2>

                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    
                    <form action="{{ route('admin.tickets.import.do') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <input type="file" name="arquivo" class="form-control" accept=".xls,.xlsx,.csv">
                          
                        <br><br>
                        
                        <button class="btn btn-success">Importar</button>

                        <a href="{{route('admin.tickets.index')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </form>

                    <div class="mt-5">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Arquivo</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Sucesso</th>
                                        <th scope="col">Falha</th>
                                        <th scope="col">Data</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @forelse ($imports as $import)
                                    <tr data-link="{{route('admin.import.failed', $import->id)}}">
                                        <th scope="row">{{$import->id}}</th>
                                        <td>{{$import->arquivo}}</td>
                                        <td>{{$import->total}}</td>
                                        <td>{{$import->sucesso}}</td>
                                        <td>{{ $import->total - $import->sucesso }}</td>
                                        <td>{{ \Carbon\Carbon::parse($import->created_at)->format('d/m/Y H:i:s') }}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="7">Nenhuma importação efetuada!</td>
                                    </tr>
                                    @endforelse
                                    
                                </tbody>
                            </table>
                        </div>
    
                        {{ $imports->onEachSide(0)->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts-footer')
    @component('components.table-click')  
    @endcomponent
@endpush