@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Tickets</h3>
                </div>

                <div class="card-body">
                    
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <h6>
                        <a href="{{route('admin.tickets.create')}}" class="btn btn-success">
                            Adicionar
                        </a>

                        <a href="{{route('admin.tickets.import.index')}}" class="btn btn-primary">
                            Importar
                        </a>
                        
                        <a href="{{route('admin.home')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </h6>
                    <div class="table-responsive">
                        <table class="table table-hover text-center">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Ticket</th>
                                    <th scope="col">SKU</th>
                                    <th scope="col">Produto</th>
                                    <th scope="col">Utilizado</th>
                                    <th scope="col">Origem</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @forelse ($tickets as $ticket)
                                <tr data-link="{{route('admin.tickets.edit', $ticket->id)}}">
                                    <th scope="row">{{$ticket->id}}</th>
                                    <td>{{$ticket->ticket}}</td>
                                    <td>{{$ticket->produto->sku}}</td>
                                    <td>{{$ticket->produto->produto}}</td>
                                    <td>{{($ticket->utilizado) ? 'Sim' : 'Não'}}</td>
                                    <td>{{($ticket->origem) ? 'Import.' : 'Manual'}}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="7">Nenhum ticket cadastrado!</td>
                                </tr>
                                @endforelse
                                
                            </tbody>
                        </table>
                    </div>

                    {{ $tickets->onEachSide(0)->links() }}

                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts-footer')
    @component('components.table-click')  
    @endcomponent
@endpush