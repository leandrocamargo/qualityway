@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2 class="mb-4">{{(isset($ticket)) ? "Editar" : "Adicionar"}} ticket</h2>


                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    
                    <form method="POST" action="{{ (isset($ticket)) ? route("admin.tickets.update", $ticket->id)  :  route("admin.tickets.store")}}">
                        @csrf
                        @if(isset($ticket))
                        @method('PUT')
                        @endif
                        <div class="form-row">
                            
                            <div class="form-group col-md-3">
                                <label for="inputTicket">Ticket</label>
                                <input type="text" name="ticket" class="form-control" value="{{ (isset($ticket)) ? $ticket->ticket : old('ticket') }}">
                            </div>

                            <div class="form-group col-md-8">
                                <label for="inputSKU">Produto</label>
                                <select class="form-control" name="produto_id">
                                    <option value>Selecione uma opção</option>
                                    @forelse ($produtos as $produto)
                                    <option value="{{$produto->id}}" {{ (isset($ticket) ? ($ticket->produto_id == $produto->id ? 'selected' : '') : (old('produto_id') == $produto->id) ? 'selected' : '')  }}>{{ $produto->sku }} - {{ $produto->produto }}</option>
                                    @empty
                                        <option value>Não existem produtos cadastrado</option>
                                    @endforelse
                                </select>
                            </div>

                            <div class="form-group col-md-1">
                                <label for="inputProduto"></label>
                                <div class="form-check mt-2">
                                    <input class="form-check-input" type="checkbox" name="utilizado" {{ (isset($ticket) ? ($ticket->utilizado ? 'checked' : '')  : (old('utilizado') ? 'checked' : '') ) }} {{(isset($ticket)) ? '' : 'disabled' }}>
                                    <label class="form-check-label" for="inputProduto">
                                        Utilizado
                                    </label>
                                </div>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary">{{(isset($ticket)) ? 'Alterar' : 'Cadastrar'}}</button>

                        <a href="{{route('admin.tickets.index')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
