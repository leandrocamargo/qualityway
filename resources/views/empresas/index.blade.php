@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Empresas</h3>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                <div class="card-body">
                    <h6>
                        <a href="{{route('admin.empresas.create')}}" class="btn btn-success">
                            Adicionar
                        </a>

                        <a href="{{route('admin.home')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </h6>
                    <div class="table-responsive">
                        <table class="table table-hover text-center">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">CNPJ</th>
                                    <th scope="col">Empresa</th>
                                    <th scope="col">RD</th>
                                    <th scope="col">URL</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @forelse ($empresas as $empresa)
                                <tr data-link="{{route('admin.empresas.edit', $empresa->id)}}">
                                    <th scope="row">{{$empresa->id}}</th>
                                    <td>{{$empresa->cnpj}}</td>
                                    <td>{{$empresa->nome}}</td>
                                    <td>{{$empresa->rd}}</td>
                                    <td>{{$empresa->nome_url}}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5">Nenhuma empresa cadastrada!</td>
                                </tr>
                                @endforelse
                                
                            </tbody>
                        </table>
                    </div>
                    
                    {{ $empresas->onEachSide(0)->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts-footer')
    @component('components.table-click')  
    @endcomponent
@endpush