@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2 class="mb-4">{{(isset($empresa)) ? "Editar" : "Adicionar"}} empresa</h2>

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    
                    <form method="POST" action="{{ (isset($empresa)) ? route("admin.empresas.update", $empresa->id)  :  route("admin.empresas.store")}}" enctype="multipart/form-data">
                        @csrf
                        @if(isset($empresa))
                        @method('PUT')
                        @endif
                        <div class="form-row">

                            <div class="form-group col-md-10">
                                <label for="inputNome">Nome</label>
                                <input type="text" name="nome" class="form-control" value="{{ (isset($empresa)) ? $empresa->nome : old('nome') }}">
                            </div>

                            <div class="form-group col-md-2">
                                <label for="inputCnpj">Cnpj</label>
                                <input type="text" name="cnpj" class="form-control cnpj" value="{{ (isset($empresa)) ? $empresa->cnpj : old('cnpj') }}" data-mask="00.000.000/0000-00" data-mask-reverse="true">
                            </div>
                            
                            

                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="inputUrl">Nome Url</label>
                                <input type="text" name="nome_url" class="form-control" value="{{ (isset($empresa)) ? $empresa->nome_url : old('nome_url') }}">
                            </div>

                            <div class="form-group col-md-2">
                                <label for="inputRd">RD</label>
                                <input type="text" name="rd" class="form-control" value="{{ (isset($empresa)) ? $empresa->rd : old('rd') }}">
                            </div>

                            <div class="form-group col-md-2">
                                <label for="inputSkin">Skin</label>
                                <input type="text" name="skin" class="form-control" value="{{ (isset($empresa)) ? $empresa->skin : old('skin') }}">
                            </div>

                            <div class="form-group col-md-2">
                                <label for="inputPedido">Pedido</label>
                                <input type="text" name="pedido" class="form-control" value="{{ (isset($empresa)) ? $empresa->pedido : old('pedido') }}">
                            </div>

                            <div class="form-group col-md-2">
                                <label for="inputPontoAtend">Ponto de Atendimento</label>
                                <input type="text" name="ponto_atendimento" class="form-control" value="{{ (isset($empresa)) ? $empresa->ponto_atendimento : old('ponto_atendimento') }}">
                            </div>

                            <div class="form-group col-md-2 text-center">
                                <label class="form-check-label" for="inputConsCpf">Consistir CPF</label>
                                <div>
                                    <input type="checkbox" name="consiste_cpf" value="1" {{ (isset($empresa) ? ($empresa->consiste_cpf ? 'checked' : '')  : 'checked' ) }}>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            
                            <div class="form-group col-md-4">
                                <label for="inputPersonalizado1">Personalizado 1</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">
                                        <input type="checkbox" name="valida_personalizado1" {{ (isset($empresa) ? ($empresa->valida_personalizado1 ? 'checked' : '')  : (old('valida_personalizado1') ? 'checked' : '') ) }} value="1" data-toggle="tooltip" title="Marque essa opção para consistir esse campo.">
                                        <label class="form-check-label ml-1" for="radioConsiste1">
                                            Consite
                                         </label>
                                      </div>
                                    </div>

                                    <input type="text" name="personalizado1" class="form-control" value="{{ (isset($empresa)) ? $empresa->personalizado1 : old('personalizado1') }}">
                                    
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                          <input type="checkbox" name="obriga_personalizado1" {{ (isset($empresa) ? ($empresa->obriga_personalizado1 ? 'checked' : '')  : (old('obriga_personalizado1') ? 'checked' : '') ) }} value="1" data-toggle="tooltip" title="Marque essa opção para obrigar esse campo.">
                                          <label class="form-check-label ml-1" for="checkboxObriga1">
                                             Obriga
                                          </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group col-md-4">
                                <label for="inputPersonalizado2">Personalizado 2</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">
                                        <input type="checkbox" name="valida_personalizado2" {{ (isset($empresa) ? ($empresa->valida_personalizado2 ? 'checked' : '')  : (old('valida_personalizado2') ? 'checked' : '') ) }} value="1" data-toggle="tooltip" title="Marque essa opção para consistir esse campo.">
                                        <label class="form-check-label ml-1" for="checkboxConsiste2">
                                            Consite
                                        </label>
                                      </div>
                                    </div>
                                    
                                    <input type="text" name="personalizado2" class="form-control" value="{{ (isset($empresa)) ? $empresa->personalizado2 : old('personalizado2') }}">

                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                          <input type="checkbox" name="obriga_personalizado2" {{ (isset($empresa) ? ($empresa->obriga_personalizado2 ? 'checked' : '')  : (old('obriga_personalizado2') ? 'checked' : '') ) }} value="1" data-toggle="tooltip" title="Marque essa opção para obrigar esse campo.">
                                          <label class="form-check-label ml-1" for="checkboxObriga2">
                                            Obriga
                                         </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="inputPersonalizado3">Personalizado 3</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">
                                        <input type="checkbox" name="valida_personalizado3" {{ (isset($empresa) ? ($empresa->valida_personalizado3 ? 'checked' : '')  : (old('valida_personalizado3') ? 'checked' : '') ) }} value="1" data-toggle="tooltip" title="Marque essa opção para consistir esse campo.">
                                        <label class="form-check-label ml-1" for="checkboxConsiste3">
                                            Consite
                                        </label>
                                      </div>
                                    </div>

                                    <input type="text" name="personalizado3" class="form-control" value="{{ (isset($empresa)) ? $empresa->personalizado3 : old('personalizado3') }}">

                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                          <input type="checkbox" name="obriga_personalizado3" {{ (isset($empresa) ? ($empresa->obriga_personalizado3 ? 'checked' : '')  : (old('obriga_personalizado3') ? 'checked' : '') ) }} value="1" data-toggle="tooltip" title="Marque essa opção para obrigar esse campo.">
                                          <label class="form-check-label ml-1" for="checkboxObriga3">
                                            Obriga
                                         </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea id="descricao" name="descricao" placeholder="Descrição exibida para o associado.">
                                    {{ (isset($empresa)) ? $empresa->descricao : old('descricao') }}
                                </textarea>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-9">
                                <label for="inputImagem">Imagem topo</label>
                                <input type="file" name="imagem" class="form-control" accept="image/*">
                            </div>

                            <div class="form-group col-md-1">
                                <label for="inputNome">Fundo</label>
                                <input type="color" name="cor_fundo" class="form-control" value="{{ (isset($empresa) ? $empresa->cor_fundo : (old('cor_fundo') ? old('cor_fundo') : '#75e2e9')) }}">
                            </div>

                            
                            <div class="form-group col-md-1">
                                <label for="inputNome">Esquerda</label>
                                <input type="color" name="cor_esquerda" class="form-control" value="{{ (isset($empresa) ? $empresa->cor_esquerda : (old('cor_esquerda') ? old('cor_esquerda') : '#3786bd')) }}">
                            </div>

                            <div class="form-group col-md-1">
                                <label for="inputNome">Direita</label>
                                <input type="color" name="cor_direita" class="form-control" value="{{ (isset($empresa) ? $empresa->cor_direita : (old('cor_direita') ? old('cor_direita') : '#ffffff')) }}">
                            </div>

                        </div>
                        
                        <button type="submit" class="btn btn-primary">{{(isset($empresa)) ? 'Alterar' : 'Cadastrar'}}</button>

                        <a href="{{route('admin.empresas.index')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </form>

                    @if (isset($empresa))
                            <hr>
                            <h3>Produtos</h3>
                            
                            <h6>
                                <a href="{{route('admin.empresa.produtos.create', $empresa->id)}}" class="btn btn-success">
                                    Adicionar
                                </a>
                            </h6>
                            <div class="table-responsive">
                                <table class="table table-hover text-center">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">SKU</th>
                                            <th scope="col">Produto</th>
                                            <th scope="col">Quantidade</th>
                                            <th scope="col">Benefício</th>
                                            <th scope="col">Desconto</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($empresaProdutos as $empresaProduto)
                                        <tr data-link="{{route('admin.empresa.produtos.edit', [$empresa->id, $empresaProduto->id])}}">
                                            <th scope="row">{{$empresaProduto->id}}</th>
                                            <td>{{$empresaProduto->produto->sku}}</td>
                                            <td>{{$empresaProduto->produto->produto}}</td>
                                            <td>{{$empresaProduto->quantidade}}</td>
                                            <td>{{($empresaProduto->tipo_beneficio == 0 ? 'Desconto' : 'Corporativo')}}</td>
                                            <td>{{($empresaProduto->tipo_beneficio == 0 ? $empresaProduto->desconto . '%' : '-')}}</td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="6">Nenhuma produto cadastrado!</td>
                                        </tr>
                                        @endforelse
                                        
                                    </tbody>
                                </table>
                            </div>
                            
                            {{ $empresaProdutos->onEachSide(0)->links() }}
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts-footer')
<!--TinyMCE-->
<script src="https://cdn.tiny.cloud/1/9kcoottp9q3yigxqcch35zwfz6fu9ti9uq1c66a9gsjdp36o/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<!--JQuery Mask-->
<script src="{{url('/assets/js/jquery.mask.min.js')}}"></script>

@component('components.table-click')  
@endcomponent

<script type="text/javascript">
    $.jMaskGlobals.watchDataMask= true;
    $(document).ready(function($){
        $('input:checkbox').tooltip();
        tinymce.init({
            selector: '#descricao'
        });
    })
</script>
@endpush
