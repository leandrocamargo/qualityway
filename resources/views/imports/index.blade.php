@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2 class="mb-3">
                        Erros na importação
                    </h2>
                    <h3 class="mb-3">
                       Arquivo: {{$import->arquivo}} - Data: {{ \Carbon\Carbon::parse($import->created_at)->format('d/m/Y G:i') }}
                    </h3>

                    @if ($import->tabela == "tickets")
                    <a href="{{route('admin.tickets.import.index')}}" class="btn btn-secondary mb-4">
                    @elseif ($import->tabela == "Pré-aprovados")
                    <a href="{{route('admin.pre-aprovados.index')}}" class="btn btn-secondary mb-4">
                    @endif
                        Voltar
                    </a>
                    
                    <div class="table-responsive">
                        <table class="table table-hover text-center">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Linha</th>
                                    <th scope="col">Atributo</th>
                                    <th scope="col">Erros</th>
                                    <th scope="col">Valores</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @forelse ($import->faileds as $failed)
                                <tr>
                                    <th scope="row">{{$failed->id}}</th>
                                    <td>{{$failed->linha}}</td>
                                    <td>{{$failed->atributo}}</td>
                                    <td>
                                        <table class="table table-bordered table-hover" style="width:100%">
                                            @php 
                                                $erros = json_decode(json_encode($failed->erros), true);
                                            @endphp
                                            @foreach($erros as $key => $val)
                                                    <tr>
                                                        <td><b>{{ $key }}</b></td>
                                                        <td>{{   $val }}</td>
                                                    </tr>
                                            @endforeach
                                            </table>
                                    </td>
                                    <td>
                                        <table class="table table-bordered table-hover" style="width:100%">
                                        @php 
                                            $valores = json_decode(json_encode($failed->valores), true);
                                        @endphp
                                        @foreach($valores as $key => $val)
                                                <tr>
                                                    <td><b>{{ $key }}</b></td>
                                                    <td>{{   $val }}</td>
                                                </tr>
                                        @endforeach
                                        </table>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5">Nenhuma falha identificada!</td>
                                </tr>
                                @endforelse
                                
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
