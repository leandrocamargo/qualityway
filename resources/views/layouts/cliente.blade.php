<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!--JQuery-->
    <script src="{{url('/assets/js/jquery.min.js')}}"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/cliente.css') }}" rel="stylesheet">
    <style>
        .page-content { background: {{$empresa->cor_fundo}} }
        .form-v4-content .form-left { background: {{$empresa->cor_esquerda}} }
        .form-v4-content { background: {{$empresa->cor_direita}} }
        .form-v4-content .form-detail { color: #000000; }
    </style>

    @stack('script')
</head>
<body>
    <div id="app">

        <div class="page-content">
            <div class="form-v4-content">
                <div class="form-left text-center">
                    @if ($empresa->imagem && $empresa->imagem != 'default.jpg')
                    <img src="{{asset("storage/empresas/".$empresa->imagem)}}" class="imagem">
                    @endif
                    {!! $empresa->descricao !!}
                </div>

                @yield('content')

            </div>
        </div>

    </div>

    @component('components.preloader')
            
    @endcomponent

    <!--JQuery Mask-->
    <script src="{{url('/assets/js/jquery.mask.min.js')}}"></script>
    <!--Bootstrap-->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    
    <script>
        $.jMaskGlobals.watchDataMask= true;
    </script>
    @stack('scripts-footer')
</body>
</html>
