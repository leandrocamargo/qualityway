<div class="modal fade" id="modalLoader" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" data-backdrop="static" data-keyboard="false" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body text-center">
            <p id="title-preloader" class="mt-2">Aguarde o processo pode demorar até um minuto...</p>
            <img id="img-preloader" src="{{asset("assets/images/preloader.gif")}}" class="preloader-gif">
        </div>
      </div>
    </div>
</div>