@extends('layouts.app')

@section('content')

<div class="">
    <iframe name="bpm-iframe" 
            id="bpm-iframe"
            src="{{ $url }}"
            frameborder="0"
            allowfullscreen
            allowfullscreen
            scrolling="auto" 
            height="100%" 
            width="100%" 
            style="height: calc(100vh - 30px);">
    </iframe>
</div>
@endsection