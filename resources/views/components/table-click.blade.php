<script type="text/javascript">
    $(document).ready(function($){
        $('table tr td').click(function(){
            if (! $(this).text().trim() == "" ) {
                //console.log($(this).closest('tr').children('td:nth-child(1)'));
                if ($(this).closest('tr').data('link') != "") {
                    url = $(this).closest('tr').data('link');
                    window.location.href = url;
                }
            }
        });

        $('#selectAll').click(function(e){    
            var table= $(e.target).closest('table');
            $('td input:checkbox',table).prop('checked',this.checked);
        });
    });
</script>