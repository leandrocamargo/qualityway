<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
            <div class="p-4">
                <p class="mt-2">Deseja realmente <b><span id="acao"></span></b> as requisições selecionadas?</p>
                
                <textarea name="observacao" class="form-control" rows="4" cols="80" value="{{ old('telefone') }}" placeholder="Observação"></textarea>
                
                <div class="d-flex justify-content-center mb-3 cancel-rec">

                    <div class="p-2">
                        <button type="button" class="btn btn-light" data-dismiss="modal">
                            Cancelar
                        </button>
                    </div>
                    <div class="p-2">
                        <button id="confirma" type="button" class="btn btn-danger">
                            Confirmar
                        </button>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>

@push('scripts-footer')

<script>
    var acao;
    var url;

    $('#modalConfirm').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        acao = button.data('acao');
        url =  button.data('url');

        var modal = $(this);
        $("#acao").text(acao);
        $("textarea[name='observacao']").val('');
        $("input[name='observacao']").val('');
    });

    $("#confirma").click(function() {
        $("#modalConfirm").modal("hide");
        $('#modalLoader').modal('show');
        
        $("input[name='observacao']").val( $("textarea[name='observacao']").val() );

        if (url != "")
            $('#table').attr('action', url);
            
        $('#table').submit();
    });
</script>
    
@endpush