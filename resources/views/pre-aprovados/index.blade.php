@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Pré-aprovados</h3>
                </div>

                <div class="card-body">
                    
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                    <form id="form" method="POST" action="{{route('admin.pre-aprovados.redirect')}}">
                    @csrf
                    <h6>
                        <div class="form-group">
                            <label for="inputSKU">Empresa</label>
                            <select class="form-control" name="empresa_id">
                                <option value>Selecione uma opção</option>
                                @forelse ($empresas as $empresa)
                                <option value="{{$empresa->id}}" {{(isset($filtro["empresa"]) && $filtro["empresa"] == $empresa->id ? 'selected' : '')}} {{ (isset($empresa) ? ($empresa->empresa_id == $empresa->id ? 'selected' : '') : (old('empresa_id') == $empresa->id) ? 'selected' : '')  }}>{{ $empresa->nome }}</option>
                                @empty
                                    <option value>Não existem empresas cadastrada</option>
                                @endforelse
                            </select>
                        </div>
                        
                        <button type="" class="btn btn-success" name="action" value="create">
                            Adicionar
                        </button>

                        <button type="" class="btn btn-primary" name="action" value="import">
                            Importar
                        </button>
                        
                        <a href="{{route('admin.home')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </h6>
                    </form>

                    <div class="d-flex flex-wrap bd-highlight mb-3">
                        <div class="p-2 bd-highlight">
                            <a href="" class="btn btn-danger btn-sm mb-1" data-toggle="modal" data-target="#modalConfirm" data-acao="EXCLUIR" data-url="">
                                Excluir
                            </a>
                        </div>
                    </div>

                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">Filtro</h5>
                            <form id="formFiltro" class="form">
                                @if (Request::has('empresa'))
                                <input type="hidden" name="empresa" value="{{ Request::get('empresa')}}">    
                                @endif
                                
                                <div class="d-flex flex-wrap justify-content-center bd-highlight mb-3">

                                    <div class="col-md-2">
                                        <select class="form-control" name="filtrarPor">
                                            <option value>Selecione uma opção</option>
                                            <option value='id' {{($filtro["filtrarPor"] == "id") ? "selected" : "" }}>Código</option>
                                            <option value='nome' {{($filtro["filtrarPor"] == "nome") ? "selected" : "" }}>Nome</option>
                                            <option value='utilizado' {{($filtro["filtrarPor"] == "utilizado") ? "selected" : "" }}>Utilizado</option>
                                            <option value='origem' {{($filtro["filtrarPor"] == "origem") ? "selected" : "" }}>Origem</option>
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <select class="form-control" name="operadorFiltro">
                                            <option value='=' {{($filtro["operadorFiltro"] == "=") ? "selected" : "" }}>É igual a...</option>
                                            <option value='!=' {{($filtro["operadorFiltro"] == "!=") ? "selected" : "" }}>É diferente de...</option>
                                            <option value='0' {{($filtro["operadorFiltro"] == "0") ? "selected" : "" }}>Começa com...</option>
                                            <option value='1' {{($filtro["operadorFiltro"] == "1") ? "selected" : "" }}>Termina com...</option>
                                            <option value='2' {{($filtro["operadorFiltro"] == "2") ? "selected" : "" }}>Contém...</option>
                                            <option value='3' {{($filtro["operadorFiltro"] == "3") ? "selected" : "" }}>Não contém...</option>
                                        </select>
                                    </div>

                                    <div class="col-md-7">
                                        <input type="text" name="valorFiltro" class="form-control" value="{{ $filtro["valorFiltro"] }}">
                                    </div>

                                    <div class="col-md-1">
                                        <button type="submit" class="btn btn-dark">Filtrar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <div class="table-responsive">
                            <form id="table" action="{{route('admin.pre-aprovados.destroy')}}" method="POST">
                            @csrf
                            <table class="table table-hover text-center">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th>&nbsp<input class="form-check-input" type="checkbox" id="selectAll"></th>
                                        <th scope="col">Nome</th>
                                        <th scope="col">Empresa</th>
                                        <th scope="col">Utilizado</th>
                                        <th scope="col">Origem</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @forelse ($preAprovados as $preAprovado)
                                    <tr data-link="{{route('admin.pre-aprovados.edit', $preAprovado->id)}}">
                                        <th scope="row">{{$preAprovado->id}}</th>
                                        <td>
                                            @if (!$preAprovado->utilizado)
                                            &nbsp<input name="id[]" class="form-check-input" type="checkbox" value="{{$preAprovado->id}}">
                                            @endif
                                        </td>
                                        <td>{{$preAprovado->nome}}</td>
                                        <td>{{$preAprovado->empresa->nome}}</td>
                                        <td>{{($preAprovado->utilizado) ? "Sim" : "Não"}}</td>
                                        <td>{{($preAprovado->origem) ? 'Import.' : 'Manual'}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="6">Nenhum Pré-aprovado cadastrado!</td>
                                    </tr>
                                    @endforelse
                                    
                                </tbody>
                            </table>
                        </form>
                    </div>
                    {{ $preAprovados->onEachSide(0)->appends([Request::except('page')])->links() }}

                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts-footer')

@component('components.table-click')  
@endcomponent
<script type="text/javascript">
    $(document).ready(function($){
        $('#selectAll').click(function(e){    
            var table= $(e.target).closest('table');
            $('td input:checkbox',table).prop('checked',this.checked);
        });

        $('select[name="empresa_id"]').change(function(){
            //console.log($(this).children('option:selected').data('url'));
            var url = "{{ route('admin.pre-aprovados.index') }}";        
            
            if ($(this).children('option:selected').val())
                url+='?empresa='+encodeURIComponent($(this).children('option:selected').val());
            
            url = url.replace(/\&$/,'');
            
            window.location.href=url;
        });
    });
</script>

@endpush

@component('components.modal')
    
@endcomponent

@component('components.preloader')
            
@endcomponent