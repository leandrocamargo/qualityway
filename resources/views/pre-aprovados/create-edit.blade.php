@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2 class="mb-4">{{(isset($preAprovado)) ? "Editar" : "Adicionar"}} Pré-aprovado</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    
                    <form method="POST" action="{{ (isset($preAprovado)) ? route("admin.pre-aprovados.update", $preAprovado->id)  :  route("admin.pre-aprovados.store")}}">
                        @csrf
                        @if(isset($preAprovado))
                        @method('PUT')
                        @endif
                        <input type="hidden" name="empresa_id" value="{{$empresa->id}}">
                        <div class="form-row">
                            
                            <div class="form-group col-md-2">
                                <label for="inputCpf" class="{{($empresa->consiste_cpf ? 'required' : '')}}">Cpf</label>
                                <input type="text" name="cpf" class="form-control cpf" value="{{ (isset($preAprovado)) ? $preAprovado->cpf : old('cpf') }}" data-mask="000.000.000-00" data-mask-reverse="true">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="inputNome">Nome</label>
                                <input type="text" name="nome" class="form-control" value="{{ (isset($preAprovado)) ? $preAprovado->nome : old('nome') }}">
                            </div>

                            <div class="form-group col-md-2">
                                <label for="inputCelular">Celular</label>
                                <input type="text" name="celular" class="form-control" value="{{ (isset($preAprovado)) ? $preAprovado->celular : old('celular') }}" data-mask="(00) 00000-0000">
                            </div>

                            
                            <div class="form-group col-md-2">
                                <label for="inputTelefone">Telefone</label>
                                <input type="text" name="telefone" class="form-control" value="{{ (isset($preAprovado)) ? $preAprovado->telefone : old('telefone') }}" data-mask="(00) 0000-0000">
                            </div>

                        </div>
                        
                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for="inputEmail">Email</label>
                                <input type="" name="email" class="form-control" value="{{ (isset($preAprovado)) ? $preAprovado->email : old('email') }}">
                            </div>
                            
                            <div class="form-group col-md-5">
                                <label for="inputSKU">Produto</label>
                                <select class="form-control" name="produto_id">
                                    <option value>Selecione uma opção</option>
                                    @forelse ($empresa->produtos as $produto)
                                    <option value="{{$produto->id}}" {{ (isset($preAprovado) ? ($preAprovado->produto_id == $produto->id ? 'selected' : '') : (old('produto_id') == $produto->id) ? 'selected' : '')  }}>{{ $produto->sku }} - {{ $produto->produto }}</option>
                                    @empty
                                        <option value>Não existem produtos cadastrado</option>
                                    @endforelse
                                </select>
                            </div>
                            
                            <div class="form-group col-md-1">
                                <label for="inputProduto"></label>
                                <div class="form-check mt-2">
                                    <input class="form-check-input" type="checkbox" name="utilizado" {{ (isset($preAprovado) ? ($preAprovado->utilizado ? 'checked' : '')  : (old('utilizado') ? 'checked' : '') ) }} {{(isset($preAprovado)) ? '' : 'disabled' }}>
                                    <label class="form-check-label" for="inputProduto">
                                        Utilizado
                                    </label>
                                </div>
                            </div>

                        </div>

                        <div class="form-row">
                            @for ($i = 1; $i <= 3; $i++)
                                @if ($empresa->{"personalizado".$i})
                            <div class="form-group col-md-4">
                                <label for="inputPersonalizado{{$i}}" class="{{($empresa->{"valida_personalizado".$i} ? 'required' : '')}}">{{ ucwords(str_replace('_', ' ', $empresa->{"personalizado".$i})) }}</label>
                                <input type="text" name="personalizado{{$i}}" class="form-control" value="{{ (isset($preAprovado)) ? $preAprovado->{"personalizado".$i} : old( "personalizado".$i ) }}">
                            </div>
                                
                                @endif
                            @endfor 
                        </div>

                        <button type="submit" class="btn btn-primary">{{(isset($preAprovado)) ? 'Alterar' : 'Cadastrar'}}</button>

                        <a href="{{route('admin.pre-aprovados.index')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!--JQuery-->
<script src="{{url('/assets/js/jquery.min.js')}}"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
@endpush

@push('scripts-footer')
<!--JQuery Mask-->
<script src="{{url('/assets/js/jquery.mask.min.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.jMaskGlobals.watchDataMask= true;
    $(document).ready(function($){
        $('input:checkbox').tooltip();
    })
</script>
@endpush