@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2 class="mb-4">{{(isset($produto)) ? "Editar" : "Adicionar"}} produto</h2>


                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    
                    <form method="POST" action="{{ (isset($produto)) ? route("admin.produtos.update", $produto->id)  :  route("admin.produtos.store")}}">
                        @csrf
                        @if(isset($produto))
                        @method('PUT')
                        @endif
                        <div class="form-row">

                            <div class="form-group col-md-3">
                                <label for="inputSKU">SKU</label>
                                <input type="text" name="sku" class="form-control" value="{{ (isset($produto)) ? $produto->sku : old('sku') }}">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="inputProduto">Produto</label>
                                <input type="text" name="produto" class="form-control" value="{{ (isset($produto)) ? $produto->produto : old('produto') }}">
                            </div>

                            <div class="form-group col-md-1 text-center">
                                <label for="inputProduto">Vídeo Conf.</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="vid_conf" value="1" data-toggle="tooltip" title="Disponível videoconferência." {{ (isset($produto) ? ($produto->vid_conf ? 'checked' : '')  : (old('vid_conf') ? 'checked' : '') ) }}>
                                  </div>
                            </div>

                            <div class="form-group col-md-2 text-center">
                                <label for="inputProduto">Biometria</label>                                
                                <div>
                                    <label class="radio-inline" for="radioSim">
                                        <input type="radio" name="biometria" value="1" class="ml-1" {{ (isset($produto) ? ($produto->biometria == 1 ? 'checked' : '')  : (old('biometria') == 1 ? 'checked' : '') ) }}> Sim
                                    </label>
                                    
                                    <label class="radio-inline" for="radioNao">
                                        <input type="radio" name="biometria" value="0" class="ml-1" {{ (isset($produto) ? ($produto->biometria == 0 ? 'checked' : '')  : 'checked'  ) }}> Não
                                    </label>
                                    
                                    <label class="radio-inline" for="radioTalvez">
                                        <input type="radio" name="biometria" value="2" class="ml-1" {{ (isset($produto) ? ($produto->biometria == 2 ? 'checked' : '')  : (old('biometria') == 3 ? 'checked' : '') ) }}> Ambos
                                    </label>
                                </div>
                                

                            </div>
                            
                        </div>

                        <button type="submit" class="btn btn-primary">{{(isset($produto)) ? 'Alterar' : 'Cadastrar'}}</button>

                        <a href="{{route('admin.produtos.index')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts-footer')
<script type="text/javascript">
    $(document).ready(function($){
        $('.form-check-input').tooltip();
    })
</script>
@endpush