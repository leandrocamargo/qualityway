@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Produtos</h3>
                </div>

                <div class="card-body">
                    
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <h6>
                        <a href="{{route('admin.produtos.create')}}" class="btn btn-success">
                            Adicionar
                        </a>
                        
                        <a href="{{route('admin.home')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </h6>
                    <div class="table-responsive">
                        <table class="table table-hover text-center">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">SKU</th>
                                    <th scope="col">Produto</th>
                                    <th scope="col" data-toggle="tooltip" title="Disponível videoconferência.">Videoconf.</th>
                                    <th scope="col" data-toggle="tooltip" title="Obrigatório biometria cadastrada.">Biometria</th>
                                </tr>
                            </thead>
                            <tbody> 
                                @forelse ($produtos as $produto)
                                <tr data-link="{{route('admin.produtos.edit', $produto->id)}}">
                                    <th scope="row">{{$produto->id}}</th>
                                    <td>{{$produto->sku}}</td>
                                    <td>{{$produto->produto}}</td>
                                    <td>{{($produto->vid_conf) ? 'Sim' : 'Não'}}</td>
                                    <td>
                                        @switch($produto->biometria)
                                            @case(1)
                                                Sim
                                                @break
                                            @case(2)
                                                Ambos
                                                @break
                                            @default
                                                Não
                                        @endswitch
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5">Nenhum produto cadastrado!</td>
                                </tr>
                                @endforelse
                                
                            </tbody>
                        </table>
                    </div>
                    {{ $produtos->onEachSide(0)->links() }}

                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts-footer')
    @component('components.table-click')  
    @endcomponent

    <script type="text/javascript">
        $(document).ready(function($){
            $('table thead tr th').tooltip();
        })
    </script>
@endpush