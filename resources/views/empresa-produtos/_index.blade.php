@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Empresas</h3>
                </div>

                <div class="card-body">
                    <h6>
                        <a href="{{route('admin.empresas.create')}}" class="btn btn-success">
                            Adicionar
                        </a>

                        <a href="{{route('admin.home')}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </h6>
                    
                    <table class="table table-hover text-center">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">CNPJ</th>
                                <th scope="col">Empresa</th>
                                <th scope="col">RD</th>
                                <th scope="col">URL</th>
                                <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @forelse ($empresas as $empresa)
                            <tr>
                                <th scope="row">{{$empresa->id}}</th>
                                <td>{{$empresa->cnpj}}</td>
                                <td>{{$empresa->nome}}</td>
                                <td>{{$empresa->rd}}</td>
                                <td>{{$empresa->nome_url}}</td>
                                <td>
                                    <a href="{{route('admin.empresas.edit', $empresa->id)}}" class="btn btn-info">
                                    Editar
                                    </a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6">Nenhuma empresa cadastrada!</td>
                            </tr>
                            @endforelse
                            
                        </tbody>
                    </table>

                    {{ $empresas->links() }}

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
