@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2 class="mb-4">{{(isset($empresaProduto)) ? "Editar" : "Adicionar"}} produto na empresa</h2>


                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    
                    <form method="POST" action="{{ (isset($empresaProduto)) ? route("admin.empresa.produtos.update", [$idEmpresa, $empresaProduto->id])  :  route("admin.empresa.produtos.store", $idEmpresa)}}">
                        @csrf
                        @if(isset($empresaProduto))
                        @method('PUT')
                        @endif
                        <div class="form-row">

                            <div class="form-group col-md-8">
                                <label for="inputSKU">Produto</label>
                                <select class="form-control" name="produto_id">
                                    <option value>Selecione uma opção</option>
                                    @forelse ($produtos as $produto)
                                    <option value="{{$produto->id}}" {{ (isset($empresaProduto) ? ($empresaProduto->produto_id == $produto->id ? 'selected' : '') : (old('produto_id') == $produto->id) ? 'selected' : '')  }}>{{ $produto->sku }} - {{ $produto->produto }}</option>
                                    @empty
                                        <option value>Não existem produtos cadastrado</option>
                                    @endforelse
                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="inputBeneficio">Tipo</label>
                                <select class="form-control" id="tipobeneficio" name="tipo_beneficio">
                                    <option value>Selecione uma opção</option>
                                    <option value="0" {{ (isset($empresaProduto) ? ($empresaProduto->tipo_beneficio == 0 ? 'selected' : '') : old('tipo_beneficio')) }}>Desconto</option>
                                    <option value="1" {{ (isset($empresaProduto) ? ($empresaProduto->tipo_beneficio == 1 ? 'selected' : '') : old('tipo_beneficio')) }}>Corporativo</option>
                                </select>
                            </div>

                            <div class="form-group col-md-1">
                                <label for="inputDesconto" style="{{(isset($empresaProduto) ? ($empresaProduto->tipo_beneficio == 0 ? '' : 'display:none') : 'display:none') }}">Desconto</label>
                                <input type="number" name="desconto" class="form-control" max="99" value="{{ (isset($empresaProduto)) ? $empresaProduto->desconto : old('desconto') }}" style="{{(isset($empresaProduto) ? ($empresaProduto->tipo_beneficio == 0 ? '' : 'display:none') : 'display:none') }}">
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="inputUrlDesc" style="{{(isset($empresaProduto) ? ($empresaProduto->tipo_beneficio == 0 ? '' : 'display:none') : 'display:none') }}">Url Desconto</label>
                                <input type="text" name="url_desconto" class="form-control" value="{{ (isset($empresaProduto)) ? $empresaProduto->url_desconto : old('url_desconto') }}" style="{{(isset($empresaProduto) ? ($empresaProduto->tipo_beneficio == 0 ? '' : 'display:none') : 'display:none') }}">
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-primary">{{(isset($empresaProduto)) ? 'Alterar' : 'Cadastrar'}}</button>

                        <a href="{{route('admin.empresas.edit', $idEmpresa)}}" class="btn btn-secondary">
                            Voltar
                        </a>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts-footer')
<script type="text/javascript">
$(document).ready(function($){
    $("#tipobeneficio").change(function(){
        var selected_option = $('#tipobeneficio').val();
        if (selected_option === '0') {
            $('label[for="inputDesconto"]').show();
            $('input[name="desconto"]').show();
            $('label[for="inputUrlDesc"]').show();
            $('input[name="url_desconto"]').show();
        } else {
            $('label[for="inputDesconto"]').hide();
            $('input[name="desconto"]').val('');
            $('input[name="desconto"]').hide();
            $('label[for="inputUrlDesc"]').hide();
            $('input[name="url_desconto"]').val('');
            $('input[name="url_desconto"]').hide();
        }
    })
})
</script>
@endpush