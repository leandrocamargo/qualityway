<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreAprovadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_aprovados', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('empresa_id');
            $table->unsignedBigInteger('produto_id')->nullable();
            $table->string('cpf', 14)->nullable();
            $table->string('nome', 50)->nullable();
            $table->string('email')->nullable();
            $table->string('celular', 15)->nullable();
            $table->string('telefone', 14)->nullable();
            $table->string('personalizado1', 30)->nullable();
            $table->string('personalizado2', 30)->nullable();
            $table->string('personalizado3', 30)->nullable();
            $table->boolean('utilizado')->default(false);
            $table->tinyInteger('origem')->default(0)->comment("0 = Manual | 1 = Importação");
            $table->timestamps();

            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('produto_id')->references('id')->on('produtos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_aprovados');
    }
}
