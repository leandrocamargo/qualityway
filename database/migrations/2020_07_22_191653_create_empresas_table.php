<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('cnpj', 18);
            $table->string('nome', 50);
            $table->string('rd', 8);
            $table->string('nome_url', 30);
            $table->string('personalizado1', 30)->nullable();
            $table->boolean('valida_personalizado1')->default(false);
            $table->boolean('obriga_personalizado1')->default(0);
            $table->string('personalizado2', 30)->nullable();
            $table->boolean('valida_personalizado2')->default(false);
            $table->boolean('obriga_personalizado2')->default(0);
            $table->string('personalizado3', 30)->nullable();
            $table->boolean('valida_personalizado3')->default(false);
            $table->boolean('obriga_personalizado3')->default(0);
            $table->boolean('consiste_cpf')->default(1);
            $table->string('imagem')->default('default.jpg');
            $table->string('cor_fundo');
            $table->string('cor_esquerda');
            $table->string('cor_direita');
            $table->text('descricao')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
