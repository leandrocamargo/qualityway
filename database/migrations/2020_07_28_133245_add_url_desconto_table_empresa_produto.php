<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUrlDescontoTableEmpresaProduto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresa_produto', function (Blueprint $table) {
            $table->text('url_desconto') // Nome da coluna
                    ->nullable()
                    ->after('desconto'); // Ordenado após a coluna
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresa_produto', function (Blueprint $table) {
            $table->dropColumn('url_desconto');
        });
    }
}
