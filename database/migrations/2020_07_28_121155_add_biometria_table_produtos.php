<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBiometriaTableProdutos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produtos', function (Blueprint $table) {
            $table->integer('biometria') // Nome da coluna
                    ->comment("0 = Não | 1 = Sim | 2 = Ambos")
                    ->default(0) // Padrão falso
                    ->after('vid_conf'); // Ordenado após a coluna "produto"
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produtos', function (Blueprint $table) {
            $table->dropColumn('biometria');
        });
    }
}
