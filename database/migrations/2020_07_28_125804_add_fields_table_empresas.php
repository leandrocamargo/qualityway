<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsTableEmpresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->string('skin', 30) // Nome da coluna
                    ->after('rd'); // Ordenado após a coluna
            $table->string('pedido', 30) // Nome da coluna
                    ->after('skin'); // Ordenado após a coluna
            $table->string('ponto_atendimento', 30) // Nome da coluna
                    ->after('pedido'); // Ordenado após a coluna
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('skin');
            $table->dropColumn('pedido');
            $table->dropColumn('ponto_atendimento');
        });
    }
}
