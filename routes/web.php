<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::view('404-tenant', 'errors.404-tenant')->name('404.tenant');

Auth::routes(['register' => true]);

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


Route::redirect('/', '/admin', 301);

Route::group(['as' => 'admin.', 'prefix' => 'admin', 'middleware' => ['auth', 'lecom.auth']], function () {
    //Home/Requisiçoes
    Route::get('/', 'AdminController@index')->name('home');
    Route::get('requisicao/{codProcesso}', 'AdminController@showRequisicao')->name('requisicao.show');
    Route::post('requisicao/{acao}', 'AdminController@acaoRequisicao')->name('requisicao.acao');

    //Manutenção produtos
    Route::resource('produtos', 'ProdutoController')->except(['destroy', 'show']);

    //Manutenção tickets
    Route::resource('tickets', 'TicketController')->except(['destroy', 'show']);
    Route::get('tickets/import', 'TicketController@import')->name('tickets.import.index');
    Route::post('tickets/import', 'TicketController@importDo')->name('tickets.import.do');

    //Manutenção empresas
    Route::resource('empresas', 'EmpresaController')->except(['destroy']);
    //Manutenção empresa produtos
    Route::resource('empresa/{id}/produtos', 'EmpresaProdutoController', ['names' => 'empresa.produtos'])->except(['index', 'destroy']);

    //Manutenção pré-aprovados
    Route::get('pre-aprovados/{idEmpresa}/create', 'PreAprovadoController@create')->name('pre-aprovados.create');
    Route::post('pre-aprovado-redirect', 'PreAprovadoController@preAprovadoRedirect')->name('pre-aprovados.redirect');
    Route::post('pre-aprovado/destroy', 'PreAprovadoController@destroy')->name('pre-aprovados.destroy');
    Route::resource('pre-aprovados', 'PreAprovadoController')->except(['create', 'destroy', 'show']);

    Route::get('pre-aprovados/{idEmpresa}/import', 'PreAprovadoController@import')->name('pre-aprovados.import.index');
    Route::post('pre-aprovados/{idEmpresa}/import', 'PreAprovadoController@importDo')->name('pre-aprovados.import.do');


    //Tela erros de importação
    Route::get('import/{id}/failed', 'ImportController@failed')->name('import.failed');

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

Route::group(['as' => 'cliente.', 'prefix' => '{cliente}', 'middleware' => ['lecom.auth']], function () {
    Route::get('/', 'ClienteController@index')->name('home');
    Route::post('/', 'ClienteController@verifyPreAprovado')->name('requisicao.verify');
    Route::get('/acompanhar-requisicao', 'ClienteController@acompanhaSolicitacao')->name('requisicao.acompanha');
    Route::post('/acompanhar-requisicao', 'ClienteController@acompanhaSolicitacaoDo')->name('requisicao.acompanha.do');
    Route::get('/{sku?}', 'ClienteController@PreAprovado')->name('requisicao.index');
    Route::post('/{sku?}', 'ClienteController@doPreAprovado')->name('requisicao.do');
});



