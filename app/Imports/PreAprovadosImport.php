<?php

namespace App\Imports;

use App\Models\Produto;
use App\Models\PreAprovado;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\WithTitle;

class PreAprovadosImport implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure
{
    use Importable, SkipsFailures;

    protected $empresa;

    public $totalCount = -1, //Subtrai a linha do cabeçalho
           $successCount = 0;

    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        
    }

    public function model(array $row)
    {        
        ++$this->successCount;

        $produto = Produto::where('SKU', $row['sku'])->first();
        
        if ($produto)
            $produto_id = $produto->id;
        else
            $produto_id = NULL;

        return new PreAprovado([
            'empresa_id'        => $this->empresa->id,
            'produto_id'        => $produto_id,
            'cpf'               => $row['cpf'],
            'nome'              => $row['nome'],
            'email'             => $row['email'],
            'celular'           => $row['celular'],
            'telefone'          => $row['telefone'],
            'personalizado1'    => $row['campo1'],
            'personalizado2'    => $row['campo2'],
            'personalizado3'    => $row['campo3'],
            'origem'            => 1,
        ]);
    }

    public function rules(): array
    { 
        ++$this->totalCount;

        return [
            'sku'               => function($attribute, $value, $onFailure) {
                                        if ($value && !$this->empresa->produtos->contains("sku",$value)) {
                                            $onFailure('Empresa não dispões do Produto/SKU.');
                                        }
                                    },
            'cpf'               => 'max:15'.($this->empresa->consiste_cpf ? '|required' : ''),
            'nome'              => 'max:50',
            'email'             => 'sometimes|nullable|email',
            'celular'           => 'max:15',
            'telefone'          => 'max:15',
            'campo1'            => 'max:30'.($this->empresa->valida_personalizado1 ? '|required' : ''),
            'campo2'            => 'max:30'.($this->empresa->valida_personalizado2 ? '|required' : ''),
            'campo3'            => 'max:30'.($this->empresa->valida_personalizado3 ? '|required' : ''),
        ];
    }
    
}
