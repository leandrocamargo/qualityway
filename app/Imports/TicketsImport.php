<?php

namespace App\Imports;

use App\Models\Produto;
use App\Models\Ticket;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;



class TicketsImport implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure
{
    use Importable, SkipsFailures;

    public $totalCount = -1, //Subtrai a linha do cabeçalho
           $successCount = 0;

    public function model(array $row)
    {
        ++$this->successCount;

        $produto = Produto::where('SKU', $row['sku'])->first();
        
        if ($produto)
            $produto_id = $produto->id;
        else
            $produto_id = 0;

        return new Ticket([
            'ticket'        => $row['ticket'],
            'produto_id'    => $produto_id,
            'origem'        => 1,
        ]);
    }

    public function rules(): array
    {
        return [
            'ticket' => 'required|max:50|unique:tickets',
            'sku' => 'exists:mysql.produtos,sku'
        ];
    }

    public function customValidationMessages()
    {
        ++$this->totalCount;

        return [
            'sku.exists' => 'Produto/SKU não localizado.',
        ];
    }
}
