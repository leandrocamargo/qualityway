<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class GestaoTicket extends Model
{

    use Sortable;

    protected $connection = 'bpm';
    protected $table = 'f_tickets';
    protected $primaryKey = 'COD_PROCESSO_F';

    public $sortable = [
                        'COD_PROCESSO_F',
                        'EMPRESA',
                        'NOME',
                        'STATUS',
                        'SKU',
                        'NOME_PRODUTO',
                        'TICKET',
                        'ATENDIMENTO',
                    ];


    public function scopeProcessos()
    {
        return $this->join("processo", function ($q){
                            $q->on("COD_PROCESSO_F", "=", "processo.COD_PROCESSO");
                            $q->on("COD_ETAPA_F", "=", "COD_ETAPA_ATUAL");
                            $q->on("COD_CICLO_F", "=", "COD_CICLO_ATUAL");
                        })
                        ->leftJoin('processo_etapa', function ($q){
                            $q->on("processo.COD_PROCESSO", "=", "processo_etapa.COD_PROCESSO");
                            $q->on("COD_ETAPA_ATUAL", "=", "COD_ETAPA");
                            $q->on("COD_CICLO_ATUAL", "=", "COD_CICLO");
                        });
    }
    
    public function produto()
    {
        return $this->hasOne(Produto::class, 'id', 'PRODUTO_ID');
    }

}
