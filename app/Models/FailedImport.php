<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FailedImport extends Model
{
    protected $connection = 'mysql';
    
    protected $fillable = ["import_id", "linha", "atributo", 'erros', "valores"];

    protected $casts = ['erros' => 'array', 'valores' => 'array'];
    
}
