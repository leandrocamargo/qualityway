<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreAprovado extends Model
{
    protected $connection = 'mysql';
    
    protected $fillable = [
        'empresa_id',
        'produto_id',
        'cpf',
        'nome',
        'email',
        'celular',
        'telefone',
        'personalizado1',
        'personalizado2',
        'personalizado3',
        'utilizado',
        'origem'
    ];

    public function empresa()
    {
        return $this->hasOne(Empresa::class, 'id', 'empresa_id');
    }

}
