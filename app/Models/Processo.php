<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Processo extends Model
{
    use Sortable;

    protected $connection = 'bpm';
    protected $table = 'processo';
    protected $primaryKey = 'COD_PROCESSO';

    public function gestaoTickets()
    {
        return $this->belongsTo(GestaoTicket::class, ['COD_PROCESSO', 'COD_ETAPA_ATUAL', 'COD_CICLO_ATUAL'], ['COD_PROCESSO_F', 'COD_ETAPA_F', 'COD_CICLO_F']);
    }
    

}
