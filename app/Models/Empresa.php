<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $connection = 'mysql';
    
    protected $fillable = [
                            "cnpj", 
                            "nome", 
                            "rd", 
                            "skin",
                            "pedido",
                            "ponto_atendimento",
                            "nome_url", 
                            "personalizado1", 
                            "valida_personalizado1", 
                            "obriga_personalizado1", 
                            "personalizado1", 
                            "valida_personalizado2", 
                            "obriga_personalizado2", 
                            "personalizado2", 
                            "valida_personalizado2",
                            "obriga_personalizado2", 
                            "personalizado3", 
                            "valida_personalizado3", 
                            "obriga_personalizado3", 
                            "consiste_cpf", 
                            "imagem",
                            "descricao",
                            "cor_fundo",
                            "cor_esquerda",
                            "cor_direita",
                        ];

    public function produtos()
    {
        return $this->belongsToMany(Produto::class)->withPivot(['tipo_beneficio', 'desconto', 'url_desconto']);
    }

    public function empresaProdutos()
    {
        return $this->hasMany(EmpresaProduto::class);
    }

}
