<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    protected $connection = 'mysql';
    
    protected $fillable = ["external_id", "tabela", "arquivo", "total", "sucesso"];

    public function faileds()
    {
        return $this->hasMany(FailedImport::class);
    }
}
