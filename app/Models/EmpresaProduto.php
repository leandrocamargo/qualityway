<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresaProduto extends Model
{
    protected $connection = 'mysql';
    
    protected $table = 'empresa_produto';
    
    protected $fillable = [
                            "empresa_id", 
                            "produto_id", 
                            "quantidade", 
                            "tipo_beneficio", //0=Desconto | 1=Gratuito
                            "desconto",
                            "url_desconto",
                        ];
    
    public function produto(){
        return $this->hasOne(Produto::class, 'id', 'produto_id');
    }
                        
}
