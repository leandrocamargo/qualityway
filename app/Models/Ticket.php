<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $connection = 'mysql';
    
    protected $fillable = ["produto_id", "ticket", "utilizado", "origem"];

    public function produto()
    {
        return $this->hasOne(Produto::class, 'id', 'produto_id');
    }

}
