<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $connection = 'mysql';
    
    protected $fillable = [
                            "sku", 
                            "produto", 
                            "vid_conf", 
                            "biometria" //0 = Não | 1 = Sim | 2 = Ambos
                        ];

    public function empresas()
    {
        return $this->hasMany(EmpresaProduto::class);
        // return $this->belongsToMany(EmpresaProduto::class);
    }
}
