<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class PsBio
{

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function verificaBiometria($cpf) {
		$onlyPersonOk = env('PSBIO_ONLY_PERSON_OK');
		$clearImages = env('PSBIO_CLEAR_IMAGES');
		$userReqVo = env('PSBIO_AGENTE_REG');
		

		$url = "vc-ivs-acbio-rest/searchByCPF";
		$headers = [
					"Content-Type" => "application/json",
					'Accept' => 'application/json',
				];
		$body = [
					"cpf" 			=> $cpf,
					"onlyPersonOk"	=> true,
					"clearImages" 	=> true,
					"userReqVo" 	=> ["cpf" => "33243134811"]
				];
		
		return $this->endpointRequest("POST", 
										$url, 
										$headers,
										json_encode($body),
										false, 
										false
									);	
	}

	public function endpointRequest($method, $url, $headers = "", $body = "", $SSL_VERIFYPEER = true, $SSL_VERIFYHOST = true)
	{
		try {
			$response = $this->client->request(
							$method, 
							env('PSBIO_BASE_URL').$url,
							[
								'curl' => array( 
									CURLOPT_SSL_VERIFYPEER => $SSL_VERIFYPEER,
									CURLOPT_SSL_VERIFYHOST => $SSL_VERIFYHOST ),
								'headers' => $headers,
								'body' => $body,
							]
						);
			return json_decode((string) $response->getBody(), true);
		} catch (\Exception $e) {
			Log::info("================================ Erro ao consultar PsBio ================================");
			Log::info("====== Body ======");
			Log::debug($body);
			Log::info("====== ./Body ======");
			Log::info("====== Erro ======");
			Log::debug($e);
			Log::info("====== ./Ero ======");
			Log::info("================================ ./Erro ao consultar PsBio ================================");
			return $e;
		}
	}

}