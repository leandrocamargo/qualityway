<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BpmLecom
{
	protected $client;
	

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function getToken() {
		$url		= "sso/api/v1/authentication";
		$headers 	= ["Content-Type" => "application/json"];
		$body 		= [
						"user" 				=> env("BPM_LECOM_USER"),
						"pass"				=> env("BPM_LECOM_PASS"),
						"keepMeLoggedIn"	=> "true"
					];
		
		return $this->endpointRequest("POST", 
										$url, 
										$headers,
										json_encode($body),
										false, 
										false
									);	
	}

	public function abreInstancia($values) {
		$arrFields = array();
		$codInstancia = env('BPM_LECOM_ID_MODELO');
		$versaoInstancia = env('BPM_LECOM_VESAO_MODELO');
		
		array_push( $arrFields,["id" => "NOME"					, "value" => $values["nome"]] );
		array_push( $arrFields,["id" => "CPF"					, "value" => $values["cpf"]] );
		array_push( $arrFields,["id" => "EMAIL"					, "value" => $values["email"]] );
		array_push( $arrFields,["id" => "CELULAR"				, "value" => $values["celular"]] );
		array_push( $arrFields,["id" => "TELEFONE"				, "value" => $values["telefone"]] );
		//array_push( $arrFields,["id" => "ATENDIMENTO"			, "value" => ($values["atendimento"]) ? 'Presencial' : 'Videoconferência'] );
		if (isset($values["personalizado1"]))
			array_push( $arrFields,["id" => "PERSONALIZADO1"	, "value" => $values["personalizado1"]] );
		if (isset($values["personalizado2"]))
			array_push( $arrFields,["id" => "PERSONALIZADO2"	, "value" => $values["personalizado2"]] );
		if (isset($values["personalizado3"]))
			array_push( $arrFields,["id" => "PERSONALIZADO3"	, "value" => $values["personalizado3"]] );
		array_push( $arrFields,["id" => "ID_EMPRESA"			, "value" => $values["empresa_id"]] );
		array_push( $arrFields,["id" => "EMPRESA"				, "value" => $values["empresa_nome"]] );
		array_push( $arrFields,["id" => "NOME_URL"				, "value" => $values["nome_url"]] );
		array_push( $arrFields,["id" => "PRODUTO_ID"			, "value" => $values["produto_id"]] );
		array_push( $arrFields,["id" => "SKU"					, "value" => $values["sku"]] );
		array_push( $arrFields,["id" => "NOME_PRODUTO"			, "value" => $values["nome_produto"]] );
		array_push( $arrFields,["id" => "OBSERVACAO"			, "value" => $values["observacao"]] );
		array_push( $arrFields,["id" => "PSBIO"					, "value" => $values["psBio"]] );
		
		$arrFields = array("values" => $arrFields);
		$headers =  [
			"ticket-sso" => session('ticket-sso'),
			"Content-Type" => "application/json",
			"language" => "pt-BR",
		];

        return $this->endpointRequest("PUT", 
										"bpm/api/v1/process-definitions/".$codInstancia."/versions/".$versaoInstancia."/start", 
										$headers,
										json_encode($arrFields),
										false, 
										false
										);
	}

	public function executaInstancia($params) {
		$arrFields = array();
		$codInstancia = env('BPM_LECOM_ID_MODELO');
		$versaoInstancia = env('BPM_LECOM_VESAO_MODELO');
		
		$headers =  [
			"ticket-sso" => session('ticket-sso'),
			"Content-Type" => "application/json",
			"language" => "pt-BR",
		];
		array_push( $arrFields, ["id" => "STATUS", "value" => $params["status"] ] );
		if ( isset( $params["ticket"]) ) {
			array_push( $arrFields, ["id" => "TICKET", "value" => $params["ticket"] ] );
			array_push( $arrFields, ["id" => "URL_AGENDAMENTO", "value" => $params["url"] ] );
		}
			
		$arrFields = ["action" => "P", "values" => $arrFields];
		
        return $this->endpointRequest("POST", 
										"bpm/api/v1/process-instances/".$params["codProcesso"]."/activity-instances/2/cycles/1/complete",  
										$headers,
										json_encode($arrFields),
										false, 
										false
										);
	}

	public function acaoRequisicao($params) {
		$arrFields = array();
		$codInstancia = env('BPM_LECOM_ID_MODELO');
		$versaoInstancia = env('BPM_LECOM_VESAO_MODELO');
		
		$headers =  [
			"ticket-sso" => session('ticket-sso'),
			"Content-Type" => "application/json",
			"language" => "pt-BR",
		];

		array_push( $arrFields, ["id" => "STATUS", "value" => $params["acao"] ] );
		array_push( $arrFields,["id" => "OBSERVACAO_ANALISE", "value" => $params["observacao"]] );
		if (isset($params['ticket'])) {
			array_push( $arrFields, ["id" => "TICKET", "value" => $params["ticket"] ] );
			array_push( $arrFields, ["id" => "URL_AGENDAMENTO", "value" => $params["url"] ] );
		}
			
		$arrFields = ["action" => "P", "values" => $arrFields];
		
        return $this->endpointRequest("POST", 
										"bpm/api/v1/process-instances/".$params["codProcesso"]."/activity-instances/5/cycles/1/complete",  
										$headers,
										json_encode($arrFields),
										false, 
										false
										);
	}

	
	public function finalizaRequisicao($params) {
		$arrFields = array();
		$codInstancia = env('BPM_LECOM_ID_MODELO');
		$versaoInstancia = env('BPM_LECOM_VESAO_MODELO');
		
		$headers =  [
			"ticket-sso" => session('ticket-sso'),
			"Content-Type" => "application/json",
			"language" => "pt-BR",
		];

		$arrFields = ["action" => "P", "values" => $arrFields];
		
        return $this->endpointRequest("POST", 
										"bpm/api/v1/process-instances/".$params["codProcesso"]."/activity-instances/".$params["codEtapa"]."/cycles/1/complete",  
										$headers,
										json_encode($arrFields),
										false, 
										false
										);
	}

	public function endpointRequest($method, $url, $headers = "", $body = "", $SSL_VERIFYPEER = true, $SSL_VERIFYHOST = true)
	{
		try {
			$response = $this->client->request(
							$method, 
							env('BPM_LECOM_BASE_URL').$url,
							[
								'curl' => array( 
									CURLOPT_SSL_VERIFYPEER => $SSL_VERIFYPEER,
									CURLOPT_SSL_VERIFYHOST => $SSL_VERIFYHOST ),
								'headers' => $headers,
								'body' => $body,
							]
						);
			
			return json_decode((string) $response->getBody(), true);
		} catch (\Exception $e) {
			Log::info("================================ Erro ao criar instância ================================");
			Log::info("====== Body ======");
			Log::debug($body);
			Log::info("====== ./Body ======");
			Log::info("====== Erro ======");
			Log::debug($e);
			Log::info("====== ./Ero ======");
			Log::info("================================ ./Erro ao criar instância ================================");
			return $e;
		}
	}

	public function response_handler($response)
	{
		if ($response) {
			return json_decode($response);
		}
		
		return [];
	}
}