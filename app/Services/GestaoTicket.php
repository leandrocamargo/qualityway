<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class GestaoTicket
{

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function geraTicket($params) {
		$url = "scheduler-rest/generateTicketAndURL";

		$headers = ["Content-Type" => "application/json"];
		$body = [	
					"name" 			=> $params["nome"],
					"email" 		=> $params["email"],
					"cpfCnpj" 		=> str_replace(['.','-'], '', $params["cpf"]),
					"cellPhone" 	=> str_replace(' ', '', $params["celular"]),
					"bioOk" 		=> ($params["psBio"]) ? true : false,
					"skin" 			=> $params['skin'], //"SKM017071"
					"sku" 			=> $params['sku'],
					"orderNumber"	=> $params['pedido'], //"1471588"
					"cOfficeCode"	=> $params['ponto_atendimento'], //"PASP000084",
					"partnerCode"	=> $params["rd"],
				];
		
		// Log::info("====== Body Gerar Ticket ======");
		// Log::debug($body);
		// Log::info("====== ./Body Gerar Ticket ======");
		return $this->endpointRequest("POST", 
										$url, 
										$headers,
										json_encode($body),
										false, 
										false
									);	
	}

	public function endpointRequest($method, $url, $headers = "", $body = "", $SSL_VERIFYPEER = true, $SSL_VERIFYHOST = true)
	{
		try {
			$response = $this->client->request(
							$method, 
							env('GESTAO_TICKET_BASE_URL').$url,
							[
								'curl' => array( 
									CURLOPT_SSL_VERIFYPEER => $SSL_VERIFYPEER,
									CURLOPT_SSL_VERIFYHOST => $SSL_VERIFYHOST ),
								'headers' => $headers,
								'body' => $body,
							]
						);
			return json_decode((string) $response->getBody(), true);
		} catch (\Exception $e) {
			Log::info("================================ Erro ao gerar ticket ================================");
			Log::info("====== Body ======");
			Log::debug($body);
			Log::info("====== ./Body ======");
			Log::info("====== Erro ======");
			Log::debug($e);
			Log::info("====== ./Ero ======");
			Log::info("================================ ./Erro ao gerar ticket ================================");
			return $e;
		}
	}

}