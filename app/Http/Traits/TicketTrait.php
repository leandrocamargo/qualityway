<?php

namespace App\Http\Traits;

trait TicketTrait {
    
    public function ProcessaTicket($params)
    {
        $empresa = $params["empresa"];

        $produto = $empresa->produtos->where("id",$params["produto_id"])->first();
        
        if($produto->pivot->tipo_beneficio == 0) {//Tipo do beneficio 0 = Desconto, pega a URL do Empresa_Produto
            return ['ticket' => '', 'url' => $produto->pivot->url_desconto];
        }

        $ticket = $this->gestaoTicket->geraTicket($params); //return [ticket,url]
        if (!verify_status_ok($ticket))
            return false;

        return $ticket;
    }

    public function ValidaRequest($request, $empresa)
    {
        $validatedData = $request->validate([
            'cpf'               => 'required|cpf|max:14',
            'nome'              => 'required|max:50',
            'email'             => 'email',
            'celular'           => 'required|max:15',
            'telefone'          => 'max:15',
            // 'atendimento'       => 'required',
            'personalizado1'    => 'max:30'.( ($empresa->valida_personalizado1 || $empresa->obriga_personalizado1) ? '|required' : ''),
            'personalizado2'    => 'max:30'.( ($empresa->valida_personalizado2 || $empresa->obriga_personalizado2) ? '|required' : ''),
            'personalizado3'    => 'max:30'.( ($empresa->valida_personalizado3 || $empresa->obriga_personalizado3) ? '|required' : ''),
        ], [],
        [
            "personalizado1" => str_replace('_', ' ',$empresa->personalizado1),
            "personalizado2" => str_replace('_', ' ',$empresa->personalizado2),
            "personalizado3" => str_replace('_', ' ',$empresa->personalizado3),
        ]);
    }
    
}