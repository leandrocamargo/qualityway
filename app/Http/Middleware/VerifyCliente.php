<?php

namespace App\Http\Middleware;

use App\Models\Empresa;
use Closure;

class VerifyCliente
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $empresa = Empresa::with("produtos")->where("nome_url", $request->route("cliente"))->first();

        if (!$empresa)
            return response(view("errors.404-cliente")->with(["cliente" => $request->route("cliente")]));
        
        
        $request['empresa'] = $empresa;
        return $next($request);
    }
}
