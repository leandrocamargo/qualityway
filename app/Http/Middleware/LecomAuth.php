<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\BpmLecom;

class LecomAuth
{

    protected $bpmlecom;

    public function __construct(BpmLecom $bpmlecom)
    {
    	$this->bpmlecom = $bpmlecom;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('ticket-sso')) {
            $response = $this->bpmlecom->getToken();
            session( ['ticket-sso' => $response["ticket-sso"] ]);
        }
        
        return $next($request);
    }
}
