<?php

namespace App\Http\Controllers;

use App\Imports\TicketsImport;
use App\Models\Import;
use App\Models\Produto;
use App\Models\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    
    public function index()
    {
        $tickets = Ticket::with('produto')->paginate(25);

        return view("tickets.index", compact("tickets"));
    }

    public function create()
    {
        $produtos = Produto::all(["id", "sku", "produto"]);
        
        return view("tickets.create-edit", compact("produtos"));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'ticket' => 'required|max:50|unique:tickets',
            'produto_id' => 'required'
        ]);

        Ticket::create($request->all());
   
        return redirect()->route('admin.tickets.index')
                        ->with('success','ticket criado com sucesso.');
    }

    public function edit(ticket $ticket)
    {
        $produtos = Produto::all(["id", "sku", "produto"]);

        return view('tickets.create-edit', compact('ticket', 'produtos'));
    }

    public function update(Request $request, ticket $ticket)
    {
        $validatedData = $request->validate([
            'ticket' => 'required|max:50|unique:tickets,ticket,'.$ticket->id,
            'produto_id' => 'required',
        ]);
  
        $dadosForm = $request->all();

        if (isset($dadosForm['utilizado']))
            $dadosForm['utilizado'] = 1;
        else
            $dadosForm['utilizado'] = 0;

        $ticket->update($dadosForm);
  
        return redirect()->route('admin.tickets.index')
                        ->with('success','Ticket alterado com sucesso.');
    }

    public function import()
    {
        $imports = Import::where('tabela', 'tickets')->paginate(25);

        return view('tickets.import', compact('imports'));
    }

    public function importDo(Request $request)
    {

        $validatedData = $request->validate([
            'arquivo' => 'required|max:2000|mimes:xlsx,xls,csv'
        ]);
        
        $import = new TicketsImport();
        $import->import(request()->file('arquivo'));
        
        $tableImport = Import::create([
            'tabela'    => 'tickets',
            'arquivo'   => request()->file('arquivo')->getClientOriginalName(),
            'total'     => $import->totalCount,
            'sucesso'   => $import->successCount,
        ]);
        
        foreach ($import->failures() as $failure) {
            $tableImport->faileds()->create([
                'linha'     => $failure->row(),
                'atributo'  => $failure->attribute(),
                'erros'     => $failure->errors(),
                'valores'   => $failure->values()
            ]);
        }

        return redirect()->route('admin.tickets.import.index')
                        ->with('success', 'Importação concluida!');
                        
    }

}
