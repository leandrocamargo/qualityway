<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\EmpresaProduto;
use App\Models\Produto;
use Illuminate\Http\Request;

class EmpresaProdutoController extends Controller
{
    public function create($idEmpresa)
    {
        $produtos = Produto::all();

        return view("empresa-produtos.create-edit", compact('idEmpresa', 'produtos'));
    }

    public function store($idEmpresa, Request $request)
    {
        $request['empresa_id'] = $idEmpresa;
        $request['desconto'] = ($request['desconto']) ? $request['desconto'] : 0;
        $request['quantidade'] = ($request['quantidade']) ? $request['quantidade'] : 0;
        
        $validatedData = $request->validate([
            'empresa_id'        => 'required|exists:mysql.empresas,id',
            'produto_id'        => 'required|exists:mysql.produtos,id|unique:empresa_produto,produto_id,NULL,id,empresa_id,'.$request->get('empresa_id'),
            'tipo_beneficio'    => 'required',
            'desconto'          => 'max:99',
        ]);
        
        $empresaProduto = EmpresaProduto::create($request->all());

        return redirect()->route('admin.empresas.edit', $idEmpresa)
                        ->with('success','Produto adicionado com sucesso.');
    }

    public function edit($idEmpresa, $idEmpresaProduto)
    {
        $produtos = Produto::all();
        $empresaProduto = EmpresaProduto::with('produto')->find($idEmpresaProduto);
        
        return view('empresa-produtos.create-edit', compact('idEmpresa', 'produtos', 'empresaProduto'));
    }

    public function update(Request $request, $idEmpresa, $idEmpresaProduto)
    {
        $request['empresa_id'] = $idEmpresa;
        $request['desconto'] = ($request['desconto']) ? $request['desconto'] : 0;
        $request['quantidade'] = ($request['quantidade']) ? $request['quantidade'] : 0;

        $validatedData = $request->validate([
            'empresa_id'        => 'required|exists:mysql.empresas,id',
            'produto_id'        => 'required|exists:mysql.produtos,id|unique:empresa_produto,produto_id,'.$idEmpresaProduto.',id,empresa_id,'.$request->get('empresa_id'),
            'tipo_beneficio'    => 'required',
            'desconto'          => 'max:99',
        ]);
  
        EmpresaProduto::find($idEmpresaProduto)->update($request->all());
  
        return redirect()->route('admin.empresas.edit', $idEmpresa)
                        ->with('success','Produto alterado com sucesso.');
    }

}
