<?php

namespace App\Http\Controllers;

use App\Models\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    
    public function index()
    {
        $produtos = Produto::paginate(25);
        
         
        return view("produtos.index", compact("produtos"));
    }

    public function create()
    {
        return view("produtos.create-edit");
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'sku' => 'required|max:50|unique:produtos',
            'produto' => 'required|max:255',
        ]);

        Produto::create($request->all());
   
        return redirect()->route('admin.produtos.index')
                        ->with('success','Produto criado com sucesso.');
    }

    public function edit(Produto $produto)
    {
        //dd($produto);
        return view('produtos.create-edit', compact('produto'));
    }

    public function update(Request $request, Produto $produto)
    {
        $validatedData = $request->validate([
            'sku' => 'required|max:50|unique:produtos,sku,'.$produto->id,
            'produto' => 'required|max:255',
        ]);
        
        $dadosForm = $request->all();
        
        if (isset($dadosForm['vid_conf']))
            $dadosForm['vid_conf'] = 1;
        else
            $dadosForm['vid_conf'] = 0;
  
        $produto->update($dadosForm);
        
        return redirect()->route('admin.produtos.index')
                        ->with('success','Produto alterado com sucesso.');
    }

}
