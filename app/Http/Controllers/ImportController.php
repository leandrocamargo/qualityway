<?php

namespace App\Http\Controllers;

use App\Models\Import;

class ImportController extends Controller
{
    
    
    public function failed($codImport)
    {
        $import = Import::with('faileds')->get()->find($codImport);
        //dd($import);
        return view('imports.index', compact('import'));
    }

}
