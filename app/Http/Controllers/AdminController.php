<?php

namespace App\Http\Controllers;

use App\Http\Traits\TicketTrait;
use App\Models\Empresa;
use App\Models\GestaoTicket;
use App\Services\BpmLecom;
use App\Services\GestaoTicket as ServicesGestaoTicket;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    use TicketTrait;

    protected $bpm;

    public function __construct(BpmLecom $bpm, ServicesGestaoTicket $gestaoTicket)
    {
        $this->bpm = $bpm;
        $this->gestaoTicket = $gestaoTicket;
    }
    
    public function index(Request $request)
    {
        $filtrarPor = $request->get("filtrarPor");
        $operadorFiltro = $request->get("operadorFiltro");
        $valorFiltro = $request->get("valorFiltro");

        $filtro = ["filtrarPor" => $filtrarPor, "operadorFiltro" => $operadorFiltro, "valorFiltro" => $valorFiltro];

        $requisicoes = GestaoTicket::query()
                                    ->processos()
                                    ->where("IDE_BETA_TESTE", "N")
                                    ;
        if ($filtrarPor) {
            if ($filtrarPor == "DAT_DATA") {
                if (validateDate($valorFiltro, "d/m/Y")) {
                    $valorFiltro = \Carbon\Carbon::createFromFormat('d/m/Y', $valorFiltro)->format('Y-m-d');
                } else {
                    $valorFiltro = "";
                }

            }

            $requisicoes = aplicaFiltroModel($requisicoes, $filtrarPor, $operadorFiltro, $valorFiltro);
        }

        $requisicoes = $requisicoes->sortable()->orderBy("DAT_DATA", 'desc')->paginate(25);
        
        return view('home', compact("requisicoes", "filtro"));
    }

    public function showRequisicao($codProcesso)
    {
        $requisicao = GestaoTicket::processos()->where("COD_PROCESSO_F", $codProcesso)->first();

        $empresa = Empresa::with('produtos')->find($requisicao->ID_EMPRESA);

        return view('requisicoes.index', compact("requisicao", "empresa"));
    }

    public function acaoRequisicao($acao, Request $request)
    {   
        $validatedData = $request->validate([
            'codProcesso'   => 'required|array|max:30',
        ], [
            'required'      => 'Selecione pelo menos uma opção.',
            'max'           => 'São permitidos no máximo 30 processos por operação.'
        ]);
        
        $params['acao'] = $acao;
        $params['codEtapa'] = 4; //Etapa finalizadora rejeição

        $errors = "";
        $params["observacao"] = $request->observacao;
        foreach ($request->codProcesso as $codProcesso) {
            $params['codProcesso'] = $codProcesso;
            if ($acao == "Aprovar") {
                $params['codEtapa'] = 3; //Etapa finalizadora Aprovação
                $processo = GestaoTicket::processos()->where("COD_PROCESSO_F", $codProcesso)->first();
                $empresa = Empresa::find($processo->ID_EMPRESA);
                
                $params["nome"]                 = $processo->NOME;
                $params["email"]                = $processo->EMAIL;
                $params["cpf"]                  = $processo->CPF;
                $params["celular"]              = $processo->CELULAR;
                $params["psBio"]                = $processo->PSBIO;
                $params["empresa"]              = $empresa;
                $params["rd"]                   = $empresa->rd;
                $params["skin"]                 = $empresa->skin;
                $params["pedido"]               = $empresa->pedido;
                $params["ponto_atendimento"]    = $empresa->ponto_atendimento;
                $params["produto_id"]           = $processo->PRODUTO_ID;
                $params["sku"]                  = $processo->SKU;

                $ticket = $this->ProcessaTicket($params);
                if (!isset($ticket['ticket'])) {
                    $errors .= $codProcesso . ", ";
                } else {
                    $params['ticket'] = $ticket['ticket'];
                    $params['url'] = $ticket['url'];
                }
            }

            if ( !isset($ticket) || ($acao == "Aprovar" && $ticket) ) {   
                $response = $this->bpm->acaoRequisicao($params); //Aprova/Rejeita requição
                if (!verify_status_ok($response))
                    $errors .= $codProcesso . ", ";
                
                $finalizaRequisicao = $this->bpm->finalizaRequisicao($params); //Aprova etapa finalizadora
            }
        }

        if ($errors != "")
            return redirect()->back()->withErrors("Não foi possível $acao o(s) processo(s) : " . \Str::beforeLast($errors, ',') .".");
        else
            return redirect()->back()->with('success', "Operação efetuada com sucesso!");
    }

}
