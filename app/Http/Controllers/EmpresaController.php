<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Image;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresas = Empresa::paginate(25);
        
         
        return view("empresas.index", compact("empresas"));
    }

    public function create()
    {
        return view("empresas.create-edit");
    }

    public function store(Request $request)
    {
        $request['nome_url'] = Str::slug(strtolower($request['nome_url']), "-");
        $request['personalizado1'] = Str::slug(strtolower($request['personalizado1']), "_");
        $request['personalizado2'] = Str::slug(strtolower($request['personalizado2']), "_");
        $request['personalizado3'] = Str::slug(strtolower($request['personalizado3']), "_");

        $validatedData = $request->validate([
            'cnpj'                  => 'required|max:18',//|unique:empresas',
            'nome'                  => 'required|max:50',
            'rd'                    => 'required|max:8',
            'skin'                  => 'required|max:30',
            'pedido'                => 'required|max:30',
            'ponto_atendimento'     => 'required|max:30',
            'nome_url'              => 'required|max:30|unique:empresas',
            'personalizado1'        => 'required_with:valida_personalizado1|required_with:obriga_personalizado1|max:30',
            'personalizado2'        => 'required_with:valida_personalizado2|required_with:obriga_personalizado2|max:30',
            'personalizado3'        => 'required_with:valida_personalizado3|required_with:obriga_personalizado3|max:30',
            'consiste_cpf'          => 'required_without_all:valida_personalizado1,valida_personalizado2,valida_personalizado3',
            'imagem'                => 'max:5000'
        ]);

        $dadosForm = $request->all();
        if($request->hasfile('imagem')){
            $upload = $request->imagem->store('public/empresas');
            if (!$upload) 
                return redirect()->route('admin.empresas.index', $empresa->id)
                                ->withErrors('Não foi possível armazenar a imagem.');
            $dadosForm['imagem'] = $request->imagem->hashName();
        }
        
        $dadosForm = $this->setChecks($dadosForm);
        $empresa = Empresa::create($dadosForm);

        return redirect()->route('admin.empresas.edit', $empresa->id)
                        ->with('success','Empresa criada com sucesso.');
    }

    public function edit(Empresa $empresa)
    {
        $empresaProdutos = $empresa->load('empresaProdutos', 'empresaProdutos.produto')->empresaProdutos()->paginate(25);
    //dd($empresa); 
        return view('empresas.create-edit', compact('empresa', 'empresaProdutos'));
    }

    public function update(Request $request, Empresa $empresa)
    {
        $request['nome_url'] = Str::slug(strtolower($request['nome_url']), "-");
        $request['personalizado1'] = Str::slug(strtolower($request['personalizado1']), "_");
        $request['personalizado2'] = Str::slug(strtolower($request['personalizado2']), "_");
        $request['personalizado3'] = Str::slug(strtolower($request['personalizado3']), "_");
        
        $validatedData = $request->validate([
            'cnpj'                  => 'required|max:18', //|unique:empresas,cnpj,'.$empresa->id,
            'nome'                  => 'required|max:50',
            'rd'                    => 'required|max:8',
            'skin'                  => 'required|max:30',
            'pedido'                => 'required|max:30',
            'ponto_atendimento'     => 'required|max:30',
            'nome_url'              => 'required|max:30|unique:empresas,nome_url,'.$empresa->id,
            'personalizado1'        => 'required_with:valida_personalizado1|required_with:obriga_personalizado1|max:30',
            'personalizado2'        => 'required_with:valida_personalizado2|required_with:obriga_personalizado2|max:30',
            'personalizado3'        => 'required_with:valida_personalizado3|required_with:obriga_personalizado3|max:30',
            'consiste_cpf'          => 'required_without_all:valida_personalizado1,valida_personalizado2,valida_personalizado3',
            'imagem'                => 'max:5000',
            'descricao'             => 'max:10000'
        ]);
        
        $dadosForm = $request->all();
        if($request->hasfile('imagem')){
            $upload = $request->imagem->store('public/empresas');
            if ($upload) {
                if(file_exists(storage_path('app/public/empresas/'.$empresa->imagem)))
                    unlink(storage_path('app/public/empresas/'.$empresa->imagem));
            } else
                return redirect()->route('admin.empresas.edit', $empresa->id)
                                ->withErrors('Não foi possível armazenar a imagem.');


            $dadosForm['imagem'] = $request->imagem->hashName();
        }
        
        $dadosForm = $this->setChecks($dadosForm);
        $empresa->update($dadosForm);
  
        return redirect()->route('admin.empresas.edit', $empresa->id)
                        ->with('success','Empresa alterada com sucesso.');
    }

    private function setChecks($dadosForm)
    {
        if (isset($dadosForm['valida_personalizado1']))
            $dadosForm['valida_personalizado1'] = 1;
        else
            $dadosForm['valida_personalizado1'] = 0;
        
        if (isset($dadosForm['obriga_personalizado1']))
            $dadosForm['obriga_personalizado1'] = 1;
        else
            $dadosForm['obriga_personalizado1'] = 0;
       
        if (isset($dadosForm['valida_personalizado2']))
            $dadosForm['valida_personalizado2'] = 1;
        else
            $dadosForm['valida_personalizado2'] = 0;
        
        if (isset($dadosForm['obriga_personalizado2']))
            $dadosForm['obriga_personalizado2'] = 1;
        else
            $dadosForm['obriga_personalizado2'] = 0;

        if (isset($dadosForm['valida_personalizado3']))
            $dadosForm['valida_personalizado3'] = 1;
        else
            $dadosForm['valida_personalizado3'] = 0;
        
        if (isset($dadosForm['obriga_personalizado3']))
            $dadosForm['obriga_personalizado3'] = 1;
        else
            $dadosForm['obriga_personalizado3'] = 0;
        
        if (isset($dadosForm['consiste_cpf']))
            $dadosForm['consiste_cpf'] = 1;
        else
            $dadosForm['consiste_cpf'] = 0;

        return $dadosForm;
    }

}
