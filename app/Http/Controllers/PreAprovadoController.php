<?php

namespace App\Http\Controllers;

use App\Imports\PreAprovadosImport;
use App\Models\Import;
use App\Models\Empresa;
use App\Models\PreAprovado;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class PreAprovadoController extends Controller
{
    use SoftDeletes;
    
    public function index(Request $request)
    {
        $filtrarPor = $request->get("filtrarPor");
        $operadorFiltro = $request->get("operadorFiltro");
        $valorFiltro = $request->get("valorFiltro");
        $empresa_id = $request->get("empresa");

        $filtro = ["filtrarPor" => $filtrarPor, "operadorFiltro" => $operadorFiltro, "valorFiltro" => $valorFiltro, "empresa" => $empresa_id];

        $empresas = Empresa::all(["id", "nome"]);

        $preAprovados = PreAprovado::query()
                                ->with('empresa');

        if ($filtrarPor) {
            if ($filtrarPor == "utilizado") 
                $valorFiltro = ( \Str::upper($valorFiltro) === "SIM" ? 1 : 0 );
            if ($filtrarPor == "origem" ) 
                $valorFiltro = ( \Str::upper($valorFiltro) === "MANUAL" ? 0 : 1 );

            $preAprovados = aplicaFiltroModel($preAprovados, $filtrarPor, $operadorFiltro, $valorFiltro);
        }

        if ($empresa_id)
            $preAprovados->where('empresa_id', $empresa_id);
        
        $preAprovados =   $preAprovados->paginate(25);

        return view("pre-aprovados.index", compact("preAprovados", "empresas", "filtro"));
    }

    public function preAprovadoRedirect(Request $request)
    {
        $validatedData = $request->validate([
            'empresa_id' => 'required|exists:mysql.empresas,id',
            'action' => 'required|in:create,import'
        ], [],
        [
            "empresa_id" => "empresa",
            "action" => "ação",
        ]);


        if ($request->get("action") == "create")
            return redirect()->route("admin.pre-aprovados.create", $request->get("empresa_id"));
        else
            return redirect()->route("admin.pre-aprovados.import.index", $request->get("empresa_id"));
    }

    public function create($idEmpresa)
    {
        $empresa = Empresa::with('produtos')->find($idEmpresa);

        return view("pre-aprovados.create-edit", compact("empresa"));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate(['empresa_id' => 'required|exists:mysql.empresas,id']);

        $empresa = Empresa::find($request->get("empresa_id"));
/*
$request->validate([
    'cpf'               => 'max:14|unique:pre_aprovados,cpf,NULL,id,empresa_id,'.$request->get('empresa_id').($empresa->consiste_cpf ? '|required' : ''),
            'nome'              => 'required|max:50',
            'email'             => 'sometimes|nullable|email',
            'celular'           => 'max:15',
            'telefone'          => 'max:15',
            'personalizado1'    => 'max:30'.($empresa->valida_personalizado1 ? '|required' : '').($empresa->valida_personalizado1 ? '|unique:pre_aprovados,personalizado1,NULL,id,empresa_id,'.$request->get('empresa_id') : ''),
            'personalizado2'    => 'max:30'.($empresa->valida_personalizado2 ? '|required' : '').($empresa->valida_personalizado2 ? '|unique:pre_aprovados,personalizado2,NULL,id,empresa_id,'.$request->get('empresa_id') : ''),
            'personalizado3'    => 'max:30'.($empresa->valida_personalizado3 ? '|required' : '').($empresa->valida_personalizado3 ? '|unique:pre_aprovados,personalizado3,NULL,id,empresa_id,'.$request->get('empresa_id') : ''),
]);
*/
        $validatedData = $request->validate([
            'cpf'               => 'max:14'.($empresa->consiste_cpf ? '|required' : ''),
            'nome'              => 'max:50',
            'email'             => 'sometimes|nullable|email',
            'celular'           => 'max:15',
            'telefone'          => 'max:15',
            'personalizado1'    => 'max:30'.($empresa->valida_personalizado1 ? '|required' : ''),
            'personalizado2'    => 'max:30'.($empresa->valida_personalizado2 ? '|required' : ''),
            'personalizado3'    => 'max:30'.($empresa->valida_personalizado3 ? '|required' : ''),
        ], [],
        [
            "personalizado1" => str_replace('_', ' ',$empresa->personalizado1),
            "personalizado2" => str_replace('_', ' ',$empresa->personalizado2),
            "personalizado3" => str_replace('_', ' ',$empresa->personalizado3),
        ]);
        
        PreAprovado::create($request->all());
        
        return redirect()->route('admin.pre-aprovados.index')
                        ->with('success','Pré-aprovado criado com sucesso.');
    }

    public function edit(PreAprovado $preAprovado)
    {
        $empresa = Empresa::with('produtos')->find($preAprovado->empresa_id);
        
        return view('pre-aprovados.create-edit', compact('preAprovado', 'empresa'));
    }
    
    public function destroy(Request $request)
    {

        $validatedData = $request->validate([
            'id'   => 'required|array|max:30',
        ], [
            'required'      => 'Selecione pelo menos uma opção.',
            'max'           => 'São permitidos no máximo 30 processos por operação.'
        ]);

        $errors = "";
        foreach ($request->id as $id) {
            $preAprovado = PreAprovado::find($id);
            if ($preAprovado) {
                $delete = $preAprovado->delete();
                if (!$delete)
                    $errors .= $id . ", ";    
            } else
                $errors .= $id . ", ";
        }

        if ($errors != "")
            return redirect()->back()->withErrors("Não foi possível EXCLUIR o(s) id(s) : " . \Str::beforeLast($errors, ',') .".");
        else
            return redirect()->back()->with('success', "Operação efetuada com sucesso!");
    }

    public function update(Request $request, PreAprovado $preAprovado)
    {
        $validatedData = $request->validate(['empresa_id' => 'required|exists:mysql.empresas,id']);

        $empresa = Empresa::find($request->get("empresa_id"));

        $validatedData = $request->validate([
            'cpf'               => 'max:14'.($empresa->consiste_cpf ? '|required' : ''),
            'nome'              => 'max:50',
            'email'             => 'sometimes|nullable|email',
            'celular'           => 'max:15',
            'telefone'          => 'max:15',
            'personalizado1'    => 'max:30'.($empresa->valida_personalizado1 ? '|required' : ''),
            'personalizado2'    => 'max:30'.($empresa->valida_personalizado2 ? '|required' : ''),
            'personalizado3'    => 'max:30'.($empresa->valida_personalizado3 ? '|required' : ''),
        ], [],
        [
            "personalizado1" => str_replace('_', ' ',$empresa->personalizado1),
            "personalizado2" => str_replace('_', ' ',$empresa->personalizado2),
            "personalizado3" => str_replace('_', ' ',$empresa->personalizado3),
        ]);

        $dadosForm = $request->all();

        if (isset($dadosForm['utilizado']))
            $dadosForm['utilizado'] = 1;
        else
            $dadosForm['utilizado'] = 0;
        
        $preAprovado->update($dadosForm);
  
        return redirect()->route('admin.pre-aprovados.index')
                        ->with('success','Pré-aprovado alterado com sucesso.');
    }

    public function import($idEmpresa)
    {
        $imports = Import::where('tabela', 'Pré-aprovados')
                        ->where('external_id', $idEmpresa)
                        ->paginate(25);

        return view('pre-aprovados.import', compact('idEmpresa','imports'));
    }

    public function importDo($idEmpresa, Request $request)
    {
        $request['empresa_id'] = $idEmpresa;
        $validatedData = $request->validate([
            'arquivo' => 'required|max:2000|mimes:xlsx,xls,csv',
            'empresa_id' => 'required|exists:mysql.empresas,id'
        ]);

        $empresa = Empresa::with('produtos')->find($request->get("empresa_id"));

        //dd($empresa->produtos->contains('id', '1'));
        
        $import = new PreAprovadosImport();
        $import->setEmpresa($empresa);
        $import->import(request()->file('arquivo'), "LayoutUsers");
        
        $tableImport = Import::create([
            'external_id'   => $idEmpresa,
            'tabela'        => 'Pré-aprovados',
            'arquivo'       => request()->file('arquivo')->getClientOriginalName(),
            'total'         => $import->totalCount,
            'sucesso'       => $import->successCount,
        ]);
        
        foreach ($import->failures() as $failure) {
            $tableImport->faileds()->create([
                'linha'     => $failure->row(),
                'atributo'  => $failure->attribute(),
                'erros'     => $failure->errors(),
                'valores'   => $failure->values()
            ]);
        }

        return redirect()->route('admin.pre-aprovados.import.index', $empresa->id)
                        ->with('success', 'Importação concluida!');
                        
    }

}
