<?php

namespace App\Http\Controllers;

use App\Http\Traits\TicketTrait;
use App\Models\GestaoTicket as ModelsGestaoTicket;
use App\Models\PreAprovado;
use App\Services\BpmLecom;
use App\Services\GestaoTicket;
use App\Services\PsBio;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    use TicketTrait;

    protected $psBio, $bpm;

    public function __construct(Request $request, PsBio $psBio, BpmLecom $bpm, GestaoTicket $gestaoTicket)
    {
        $this->middleware('verify.cliente');
        $this->psBio = $psBio;
        $this->bpm = $bpm;
        $this->gestaoTicket = $gestaoTicket;
    }

    public function index($empresa, Request $request)
    {
        $empresa = $request->empresa;

        return view("clientes.index", compact("empresa"));
    }

    public function verifyPreAprovado(Request $request)
    {
        $empresa = $request->empresa;
        
        $this->ValidaRequest($request, $empresa);

        $psBio = $this->psBio->verificaBiometria(str_replace(['.','-'], '', $request->cpf));

        if (!verify_status_ok($psBio))
            return redirect()->back()->withErrors("Não foi possível consultar cadastro biométrico!")->withInput();

        if ($psBio['success']) {
            $produtos = $empresa->produtos->whereIn("biometria", [1, 2]);
            $request['psBio'] = 1;
        } else {
            $produtos = $empresa->produtos->whereIn("biometria", [0, 2]);
            $request['psBio'] = 0;
        }
        
        // Não funcionou, ficará para a V2
        // if ($produtos->count() == 1) { //Efetua a requisição caso só possua um SKU
            // $request["produto_id"] = $produtos->first()->id;
            // $this->doPreAprovado($request->cliente, $produtos->first()->sku, $request);
        // } else //if ($produtos) //Direciona associado para selecionar o produto
        return response()->view("clientes.seleciona-sku", compact("request","empresa","produtos"));
        //else //Efetua a requisição
    }

    public function PreAprovado ($cliente, $sku, Request $request)
    {
        $empresa = $request->empresa;
        if (!$empresa->produtos->contains('sku', $sku))
            return response(view("errors.404-produto")->with(["cliente" => $cliente, "sku" => $sku]));
        
        $produto = $empresa->produtos->where('sku', $sku)->first();
        //Parâmetro para a página index identificar o action do form
        $doPreAprovado = true;

        return view("clientes.index", compact("empresa", "produto", "doPreAprovado"));
    }

    public function doPreAprovado($cliente, $sku, Request $request)
    {
        $validatedData = $request->validate([
            'produto_id' => 'exists:mysql.produtos,id'
        ], [],
        [
            "produto_id" => "produto",
        ]); 

        $empresa = $request->empresa;
        
        //Verifica se existe uma requisção para o CPF e SKU aberta
        $existeRequisicao = ModelsGestaoTicket::processos()
                                            ->where("CPF", $request->cpf)
                                            ->where("PRODUTO_ID", $request->produto_id)
                                            ->where("IDE_FINALIZADO", 'A')
                                            ->first();
        
        if ($existeRequisicao) {
            //Retona página com status e texto(fixo)
            $status = "Requisição já solicitada";
            $texto  = "<h5>Já foi efetuado uma solicitação do produto para esse CPF</h5>";
            $texto .= "<h5>Caso deseje, pode consultar o status a qualquer momento no link abaixo.</h5>";
            return response()->view("clientes.status", compact("empresa","ticket", "status", "texto"));
        }

        $this->ValidaRequest($request, $empresa);
        
        $request['empresa_id']          = $empresa->id;
        $request['empresa_nome']        = $empresa->nome;
        $request['rd']                  = $empresa->rd;
        $request["skin"]                = $empresa->skin;
        $request["pedido"]              = $empresa->pedido;
        $request["ponto_atendimento"]   = $empresa->ponto_atendimento;
        $request['sku']                 = $sku;    
        $request['nome_produto']        = $empresa->produtos->where("id",$request->produto_id)->first()->produto;
        $request['nome_url']            = $cliente;
        $request['sku']                 = $sku;

        //Verifica se o acesso o cliente direto pelo SKU
        if (isset($request->source)) {
            $psBio = $this->psBio->verificaBiometria(str_replace(['.','-'], '', $request->cpf));
            
            if (!verify_status_ok($psBio))
                return redirect()->back()->withErrors("Não foi possível consultar cadastro biométrico!")->withInput();

            $request["psBio"] = ($psBio['success'] ? 1 : 0);
        }
        
        $processo = $this->bpm->abreInstancia($request->all()); //return content => [processInstanceId,currentActivityInstanceId,currentCycle,uuid]
        if (!verify_status_ok($processo))
            return redirect()->back()->withErrors("Não foi possível iniciar a requisição!")->withInput();
        $request["codProcesso"] = $processo["content"]["processInstanceId"];
        
        $produto_id = $request->produto_id;
        $preAprovado = PreAprovado::query()
                                ->where("utilizado", 0)
                                ->where(function ($q) use ($produto_id)
                                {
                                    return $q->where("produto_id", $produto_id)
                                                ->orWhereNull('produto_id');
                                })
                                ->orderBy('produto_id', 'desc');
        
        if ($empresa->consiste_cpf)
            $preAprovado->where("cpf", $request->cpf);
        if ($empresa->valida_personalizado1)
            $preAprovado->where("personalizado1", $request->personalizado1);
        if ($empresa->valida_personalizado2)
            $preAprovado->where("personalizado2", $request->personalizado2);
        if ($empresa->valida_personalizado3)
            $preAprovado->where("personalizado3", $request->personalizado3);
        
        $preAprovado = $preAprovado->first();
        
        if($preAprovado){
            $ticket = $this->ProcessaTicket($request->all());

            if (!$ticket)
                return redirect()->back()->withErrors("Não foi possível gerar o ticket de agendamento!")->withInput();
            //Executa etapa informando que está pré-aprovado
            $requisicao = $this->bpm->executaInstancia(["codProcesso" => $request->codProcesso, "status" => "Pré-aprovado", "ticket" => $ticket['ticket'], "url" => $ticket['url']]); //return content => [processInstanceId,currentActivityInstanceId,currentCycle,uuid]
            //FinalizaProcesso
            $finalizaProcesso = $this->bpm->finalizaRequisicao(["codProcesso" => $request->codProcesso, "codEtapa" => "1"]);
            //Marca linha de pré-aprovado como utilizada
            $preAprovado->update(["utilizado" => 1]);
            //Renderiza página com informações do ticket/redireciona
            return response()->view("clientes.redirect", compact("empresa","ticket"));
        }   else {
            //Executa etapa informando que NÃO está pré-aprovado
            $requisicao = $this->bpm->executaInstancia(["codProcesso" => $request->codProcesso, "status" => "Sem pré-aprovação"]); //return content => [processInstanceId,currentActivityInstanceId,currentCycle,uuid]
            //Retona página com status e texto(fixo)
            $status = "Em análise";
            $texto  = "<h5>Sua requisição foi aberta e assim que for avaliada receberá um e-mail.</h5>";
            $texto .= "<h5>Caso deseje, pode consultar o status a qualquer momento no link abaixo.</h5>";

            return response()->view("clientes.status", compact("empresa","ticket", "status", "texto"));
        }
    }

    public function acompanhaSolicitacao(Request $request)
    {
        $empresa = $request->empresa;

        return view('clientes.acompanha-solicitacao', compact("empresa"));
    }

    public function acompanhaSolicitacaoDo(Request $request)
    {
        $validatedData = $request->validate([
            'cpf'               => 'required|cpf|max:14',
            'email'             => 'email',
            'celular'           => 'required|max:15',
        ]);

        $empresa = $request->empresa;

        $requisicoes = ModelsGestaoTicket::processos()
                                        ->with('produto')
                                        ->where("CPF", $request->cpf)
                                        ->where("EMAIL", $request->email)
                                        ->where("CELULAR", $request->celular)
                                        ->where("ID_EMPRESA", $empresa->id)
                                        ->where("IDE_BETA_TESTE", "N")
                                        ->orderBy('COD_PROCESSO_F', 'desc')
                                        ->get(["ID_EMPRESA", "IDE_FINALIZADO", "COD_PROCESSO_F", "COD_ETAPA_F", "PRODUTO_ID", "TICKET", "URL_AGENDAMENTO", "DAT_DATA", "OBSERVACAO_ANALISE"]);

        return response()->view("clientes.status", compact("empresa","requisicoes"));
    }
}
