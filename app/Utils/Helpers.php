<?php

if (! function_exists('create_bpm_form_url')) {
    function create_bpm_form_url($processInstanceId, $currentActivityInstanceId, $currentCycle, $uuid)
    {
        $url = env('BPM_LECOM_BASE_URL')."form-app/process-instances/activity-forms?";
        $url .= "processInstanceId=".$processInstanceId;
        $url .= "&activityInstanceId=".$currentActivityInstanceId;
        $url .= "&cycle=".$currentCycle;
        $url .= "&uuid=".$uuid;
        $url .= "&testMode=false&language=pt_BR&displayFormHeader=false&highContrast=false";

        return $url;
    }
}

if (! function_exists('verify_status_code')) {
    function verify_status_ok($object) {
        if (method_exists($object, "getCode") && $object->getCode() != 200)
            return false;
        
        return true;
    }
}

if (! function_exists('validateDate')) {
    function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}

if (! function_exists('nomeEtapa')) {
    function nomeEtapa($codEtapa)
    {
        switch ($codEtapa) {
            case 2:
                return utf8_encode('Efetuando requisição');
                break;
            
            case 1:
                return utf8_encode('Pré-aprovado');
                break;
            
            case 5:
                return utf8_encode('Analisar requisição');
                break;
            
            case 3:
                return 'Aprovado';
                break;
            
            case 4:
                return 'Rejeitado';
                break;
            
            default:
                return $codEtapa . ' - N/D';
                break;
        }
    }
}

if (! function_exists('nomeStatus')) {
    function nomeStatus($status)
    {
        switch ($status) {
            case "P":
                return 'Concluído';
                break;
            
            case "A":
                return 'Em Andamento';
                break;
            
            case "C":
                return 'Cancelado';
                break;
            
            default:
                return $status . ' - N/D';
                break;
        }
    }
}

if (! function_exists('aplicaFiltroModel')) {
    function aplicaFiltroModel($model, $filtrarPor, $operadorFiltro, $valorFiltro)
    {
        switch ($operadorFiltro) {
            case '0':
                $model->where($filtrarPor, 'like', $valorFiltro.'%');
                break;
            
            case '1':
                $model->where($filtrarPor, 'like', '%'.$valorFiltro);
                break;
            
            case '2':
                $model->where($filtrarPor, 'like', '%'.$valorFiltro.'%');
                break;
            
            case '3':
                $model->where($filtrarPor, 'NOT like', '%'.$valorFiltro.'%');
                break;
            
            default:
                $model->where($filtrarPor, $operadorFiltro, $valorFiltro);
                break;
        }

        return $model;
    }
}